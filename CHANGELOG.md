# Change Log
All notable changes to this project will be documented in this file.


## [1.10.2] - 2024-11-25

### Fixed
- Fixed issue, where scene picker would not prompt about unsaved scene changes

## [1.10.1] - 2024-11-25

### Fixed
- Fixed issue, where scene picker settings wouldn't save, if removing a group element

## [1.10.0] - 2024-11-17

### Added
- Added samples for burst and entities


## [1.9.5] - 2024-07-18

### Fixed
- Fixed layout issues in Unity 6, where height was not set to 22px on parent VisualElement


## [1.9.4] - 2024-04-18

### Added
- Added right-click pings object, but does not select object in history  


## [1.9.3] - 2024-03-23

### Fixed
- Fixed issue with missing icons for scene (single/additive) toggle button in Unity 6000.0b

## [1.9.2] - 2023-09-18

### Changed
- Scene picker icons in light mode will now be drawn as dark icons, instead of light grey


## [1.9.1] - 2023-09-10

### Fixed
- Fixed issue, where multiple canvases with the same name would only appear once in the list


## [1.9.0] - 2023-08-31

### Added
- Added toolbar button to open preferences and project settings
- Added option to add groups of scenes to open using scene picker 

### Fixed
- Fixed issue, where scene picker would display deleted or unsaved scenes and selectable or no label entries


## [1.8.5] - 2023-03-28

### Added
- Marking history elements as favorites will move them to the top of the list


## [1.8.4] - 2023-03-27

### Added
- Set selection active in history is now a toggleable feature in the context menu

### Changed
- Increased number of entries in history from 32 to 64
- Clear entries moved to context menu in history

### Fixed
- Fixed issue, where history didn't work on first startup


## [1.8.3] - 2023-03-16

### Changed
- Create new scene will now give the option to create it as single or additive


## [1.8.2] - 2023-03-14

### Added
- Create new scene next to scene picker

### Changed
- Icon on additive and single scene toggle changed to be single or multiple "window" icon


## [1.8.1] - 2022-12-07

### Changed
- Updated README.md to include all default tools


## [1.8.0] - 2022-12-07

### Added
- CHANGELOG.md
- Tooltips on all buttons and labels in toolbar tools
- Target frame rate with `[min;max]` slider in toolbar
- `PlayerPrefs` delete button added to toolbar
- Option to select Single/Additive scene load/unload to scene picker
- All `IToolbarSettings` will be grouped with a foldout-toggle in Preferences

 
## [1.7.2] - 2022-09-20

### Fixed
- Fixed issue, where changing scene by opening scene files would not refresh scene selection tool


## [1.7.1] - 2022-08-08

### Fixed
- Fixed compiler issue, due to `string.Join` with `char` not being available in Unity 2020


## [1.7.0] - 2022-06-26

### Added
- Option to sort scenes by asset labels in scene picker in toolbar


## [1.6.0] - 2022-06-25

### Added
- Position and rotation pivots for scene view toolbar menu which uses buttons, instead of toggle drop down menu


## [1.5.2] - 2022-02-15

### Fixed
- Fixed issue, where toolbar styles modified a style directly in a Unity GUISkin thereby given incorrect buttons in other windows


## [1.5.1] - 2022-02-14

### Added
- Added toolbar settings interface to readme file


## [1.5.0] - 2022-02-14

### Added
- Added interface and utility class for adding settings to toolbar settings provider


## [1.4.1] - 2022-02-13

### Fixed
- Fixed issue, where new build tools used unsupported styles in Unity 2019 and 2020


## [1.4.0] - 2022-02-13

### Added
- Build target picker and build button to toolbar


## [1.3.3] - 2022-01-28

### Added
- Setting time scale close to 1.0 in snap it to 1.0


## [1.3.2] - 2022-01-28

### Added
- Added min & max values for time slider 
- Options for max time scale value in preferences

## [1.3.1] - 2022-01-21

### Fixed
- Removing all package scenes from ScenePicker, due to read-only scenes not being loadable


## [1.3.0] - 2022-01-20

### Added
- `EditorProjectPrefs`, so each project can have their own settings for history and favorites


## [1.2.0] - 2022-01-15

### Added
- Time scale to toolbar 


## [1.1.0] - 2021-12-19

### Added
- README file with instructions about usage of the different tools
- ToolbarSettings, so visibility of tools can be set in Preferences/Module/Toolbar
- Priority for determining which order tools should be added on left and right side of play buttons


## [1.0.2] - 2021-12-17

### Fixed
- Fixed issue, where PrefabStage no longer was marked as experimental in Unity 2021


## [1.0.1] - 2021-11-19

### Changed
- Removed noisy debug log in toolbar, left in by mistake in [1.0.0]


## [1.0.0] - 2021-11-10

### Fixed
- Fixed issue, where toolbar elements could overlap with other toolbar elements in Unity 2020 and older


## [0.8.3] - 2021-10-16

### Fixed
- Fixed issue, where creating a new scene didn't set scene picker as dirty


## [0.8.2] - 2021-09-28

### Changed
- Removed scene listing from addressables, since it is too expensive (~220 times worse, then just running through entire `AssetDatabase` for scenes)


## [0.8.1] - 2021-09-26

### Fixed
- Fixed issue, where scenes in `EditorBuildSettings` and addressabels would appear multiple times, due to scenes in `EditorBuildSettings` automatically being added to addressables


## [0.8.0] - 2021-09-18

### Fixed
- Fixed issue, where a compiler error could result in a cascade of reflection exceptions from the toolbar


## [0.7.2] - 2021-09-17

### Added
- Added asset bundle and addressable scenes to scene picker


## [0.7.1] - 2021-09-13

### Changed
- Changed assembly definition name from `Game.NavigationTool.Editor` to `Module.NavigationTool.Editor`


## [0.7.0] - 2021-09-12

### Changed
- Changed namespace from `Game.*` to `Module.*`


## [0.6.6] - 2021-09-04

### Fixed
- Fixed issue, where scene picker wouldn't update correctly, when in play mode and changing active scene
- Fixed issue, where it wasn't possible to select canvas in prefab stage


## [0.6.5] - 2021-08-02

### Fixed
- Fixed issue, where canvas picker wouldn't update selection when changing scenes

## [0.6.4] - 2021-06-29

### Added
- Added log warning, if trying to focus on a canvas `NULL` object


## [0.6.3] - 2021-06-21

### Changed
- Replaced `IToolbarDrawer` with `AbstractToolbarDrawer` with rect and styles setup-handling

### Fixed
- Fixed issue, where toolbar items didn't accept mouse input in 2021 or newer versions


## [0.6.2] - 2021-04-27
 
### Added
- Added all scenes to Scene picker tool, where scenes from `EditorBuildSettings` will be marked with "(Build Settings)"-postfix


## [0.6.1] - 2021-04-12

### Fixed
- Fixed issue, where UI Canvas picker wasn't available in playmode
- Fixed issue, where UI Layer toggle wasn't available in playmode


## [0.6.0] - 2021-04-12
 
### Added
- UI Canvas picker to toolbar for easy selecting and centering a canvas in scene view
- UI Layer toggle to toolbar, so it can be turned on/off in scene view


## [0.5.0] - 2021-03-11
 
### Added
- Toolbar drawers for adding buttons next to play buttons
- Scene picker tool to toolbar for easy selecting between scenes in `EditorBuildSettings`
 
### Changed
- Keywords in manifest file to include toolbar


## [0.4.0] - 2021-02-14
 
### Added
- Double-clicking item in history window will now open asset in preferred tool

 
## [0.3.0] - 2021-02-14
 
### Added
- History window with a list of all prior selected objects
 
### Changed
- Keywords in manifest file to include favorites and history

 
## [0.2.0] - 2021-02-14

### Changed
  
- Added drag and drop between entries in favorites tree view


## [0.0.1] - 2021-02-14
 
### Added
- Favorites window with tree view, search and tools for adding, renaming, deleting items
