﻿# Description

This module contains a few helpful editor tools, like favorites, history and some handy toolbar tools.


## Favorites

An editor window, where you can pin folders and assets, so you can easily find the most used assets in your project.


## History

An editor window, where the history of all selected assets are shown. An asset in the window can be selected to easy get back to previous selections.


## Toolbar

Toolbars to the left and right of the play-buttons. Anything can be added, as long as it is supported by IMGUI.


### Default tools

Several tools are available from start:

* UI Layer: Toggle the UI layer on/off
* UI Canvas: Select and center the camera on any Canvases in the scene
* Scene: Load any scene in the project easily by using the dropdown
* Time Scale: Adjust the time scale from [0;1] from a slider
* Target Frame Rate Scale: Adjust target frame rate from [10;144] from a slider
* Player Prefs: Delete all PlayerPrefs data


### Customization

You can create your own toolbar extensions by extending `AbstractToolbarDrawer`.

```
internal sealed class MyCustomTool : AbstractToolbarDrawer
{
    public override bool Visible => true;
    public override bool Enabled => true;
    public override EToolbarPlacement Placement => EToolbarPlacement.Left;
    public override int Priority => (int)EToolbarPriority.Medium;

    protected override void Draw(Rect rect)
    {
        // Use IMGUI methods for drawing
    }

    public override float CalculateWidth()
    {
        // Default width for toolbar buttons is 30
        return 30.0f;
    }
}
```

The four properties help define draw and enable state:

* `Visible`: If it is visible in the toolbar
* `Enabled`: If it is enabled in the toolbar (`GUI.enabled`)
* `Placement`: If it is placed to the left or right of the play-buttons
* `Priority`: Sort order/priority, when drawing the tool


### Customization - Settings

To add enable/disable settings or more to Preferences -> Toolbar, you can implement `IToolbarSettings`.

```
internal sealed class ToolbarUiSettings : IToolbarSettings
{
    public string Title => "UI";
    
    private const string PREF_IS_UI_ENABLED = "ToolbarSettings_IsUiEnabled";
    private const string PREF_IS_UI_LAYER_ENABLED = "ToolbarSettings_IsUiLayerEnabled";
    
    public static bool IsUiEnabled
    {
        get => EditorPrefs.GetBool(PREF_IS_UI_ENABLED, false);
        set => EditorPrefs.SetBool(PREF_IS_UI_ENABLED, value);
    }

    public static bool IsUiLayerEnabled
    {
        get => EditorPrefs.GetBool(PREF_IS_UI_LAYER_ENABLED, false);
        set => EditorPrefs.SetBool(PREF_IS_UI_LAYER_ENABLED, value);
    }

    public void Draw()
    {
        IsUiEnabled = EditorGUILayout.Toggle("Enable Canvas picker", IsUiEnabled);
        IsUiLayerEnabled = EditorGUILayout.Toggle("Enable Layer toggle", IsUiLayerEnabled);
    }
}
```