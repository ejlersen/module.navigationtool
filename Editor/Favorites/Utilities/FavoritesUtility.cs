﻿using System.Collections.Generic;
using Module.NavigationTool.Editor.Utilities;
using UnityEditor;

namespace Module.NavigationTool.Editor.Favorite
{
    internal static class FavoritesUtility
    {
        private static Favorites FAVORITES;

        static FavoritesUtility()
        {
            FAVORITES = null;
        }
        
        public static Favorites GetFavorites()
        {
            return FAVORITES ?? (FAVORITES = new Favorites());
        }

        public static bool IsLoaded()
        {
            return FAVORITES != null;
        }

        public static void RefreshAll()
        {
            if (FAVORITES == null)
                return;

            List<Favorites.Entry> entries = FAVORITES.entries;

            for (var i = 0; i < entries.Count; i++)
            {
                entries[i].Refresh();
            }
        }

        [MenuItem("Tools/Utilities/Favorites/Delete")]
        public static void DeleteAll()
        {
            FAVORITES = null;
            EditorProjectPrefs.DeleteKey(Favorites.PREF_ID);
        }
    }
}