﻿using UnityEditor;

namespace Module.NavigationTool.Editor.Favorite
{
    internal sealed class FavoritesPostProcess : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            bool isDirty = deletedAssets.Length != 0 ||
                           movedAssets.Length != 0 ||
                           movedFromAssetPaths.Length != 0;

            if (isDirty && FavoritesUtility.IsLoaded())
                FavoritesUtility.RefreshAll();
        }
    }
}