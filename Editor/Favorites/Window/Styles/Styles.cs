﻿using UnityEngine;

namespace Module.NavigationTool.Editor.Favorite
{
    internal sealed class Styles
    {
        public GUIStyle toolbox;
        public GUIStyle buttonAddFolder;
        
        public GUIStyle entry;
        public GUIStyle invalidEntry;
        public Texture2D foldoutIn;
        public Texture2D foldoutOut;
        
        public GUIStyle contextOption;

        private GUISkin skin;

        public void Initialize(GUISkin skin)
        {
            if (this.skin == skin)
                return;

            this.skin = skin;
            toolbox = new GUIStyle(skin.box);

            buttonAddFolder = new GUIStyle(skin.button);
            buttonAddFolder.padding = new RectOffset(4, 0, 0, 4);

            entry = new GUIStyle(skin.label);
            entry.hover.textColor = entry.onHover.textColor = Color.white;
            entry.active.textColor = entry.onActive.textColor = Color.yellow;

            invalidEntry = new GUIStyle(entry);
            invalidEntry.normal.textColor = Color.red;
            invalidEntry.hover.textColor = invalidEntry.onHover.textColor = new Color(1.0f, 0.3f, 0.3f);
            invalidEntry.active.textColor = invalidEntry.onActive.textColor = new Color(1.0f, 0.0f, 0.5f);

            GUIStyle style = skin.FindStyle("Foldout");

            if (style != null)
            {
                foldoutIn = style.normal.scaledBackgrounds[0];
                foldoutOut = style.onNormal.scaledBackgrounds[0];
            }

            contextOption = new GUIStyle(skin.label);
            contextOption.hover.textColor = contextOption.onHover.textColor = Color.white;
            contextOption.hover.scaledBackgrounds = contextOption.onHover.scaledBackgrounds = skin.box.normal.scaledBackgrounds;
            contextOption.active.textColor = contextOption.onActive.textColor = Color.yellow;
            contextOption.active.scaledBackgrounds = contextOption.onActive.scaledBackgrounds = skin.box.normal.scaledBackgrounds;
        }
    }
}