﻿using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Favorite
{
    internal sealed class EditorFavoritesPopupWindowAddFolder : PopupWindowContent
    {
        private readonly Favorites.Entry toParent;
        private string inputStr = string.Empty;

        public static void Show(Vector2 mousePosition, Favorites.Entry toParent)
        {
            var rect = new Rect(mousePosition.x - 200.0f, mousePosition.y, 0.0f, 0.0f);
            PopupWindow.Show(rect, new EditorFavoritesPopupWindowAddFolder(toParent));
        }

        private EditorFavoritesPopupWindowAddFolder(Favorites.Entry toParent)
        {
            this.toParent = toParent;
        }
        
        public override void OnGUI(Rect rect)
        {
            bool isReturn = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return;
            bool isEscape = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Escape;

            GUI.SetNextControlName("TextField");
            inputStr = GUI.TextField(rect, inputStr);
            GUI.FocusControl("TextField");
            
            if (isReturn && !string.IsNullOrEmpty(inputStr))
            {
                Favorites favorites = FavoritesUtility.GetFavorites();
                favorites.AddFolder(inputStr, toParent);
                editorWindow.Close();
            }
            
            if (isEscape)
                editorWindow.Close();
        }

        public override Vector2 GetWindowSize()
        {
            return new Vector2(200.0f, EditorGUIUtility.singleLineHeight);
        }
    }
}