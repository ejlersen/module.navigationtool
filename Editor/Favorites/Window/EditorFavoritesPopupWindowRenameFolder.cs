﻿using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Favorite
{
    internal sealed class EditorFavoritesPopupWindowRenameFolder : PopupWindowContent
    {
        private readonly Favorites.Entry entry;
        private string inputStr;

        public static void Show(Vector2 mousePosition, Favorites.Entry entry)
        {
            var rect = new Rect(mousePosition.x - 200.0f, mousePosition.y, 0.0f, 0.0f);
            PopupWindow.Show(rect, new EditorFavoritesPopupWindowRenameFolder(entry));
        }

        private EditorFavoritesPopupWindowRenameFolder(Favorites.Entry entry)
        {
            this.entry = entry;
            inputStr = entry.name;
        }
        
        public override void OnGUI(Rect rect)
        {
            bool isReturn = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return;
            bool isEscape = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Escape;

            GUI.SetNextControlName("TextField");
            inputStr = GUI.TextField(rect, inputStr);
            GUI.FocusControl("TextField");
            
            if (isReturn && !string.IsNullOrEmpty(inputStr))
            {
                entry.name = inputStr;
                entry.lowerName = inputStr.ToLower();
                entry.content.text = inputStr;

                Favorites favorites = FavoritesUtility.GetFavorites();
                favorites.Save();
                
                editorWindow.Close();
            }
            
            if (isEscape)
                editorWindow.Close();
        }

        public override Vector2 GetWindowSize()
        {
            return new Vector2(200.0f, EditorGUIUtility.singleLineHeight);
        }
    }
}