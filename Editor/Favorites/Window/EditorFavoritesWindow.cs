﻿using System;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Favorite
{
    internal sealed class EditorFavoritesWindow : EditorWindow
    {
        [NonSerialized]
        private Styles styles;
        [SerializeField]
        public EditorFavoritesViewTools viewTools;
        [SerializeField]
        private EditorFavoritesViewTreeList viewTreeList;
        [SerializeField]
        private EditorFavoritesViewSearchList viewSearchList;
        
        [MenuItem("Tools/Windows/Favorites")]
        public static void Open()
        {
            var window = GetWindow<EditorFavoritesWindow>();
            window.titleContent = new GUIContent("Favorites");
            window.Show();
        }

        private void OnEnable()
        {
            if (styles == null)
                styles = new Styles();
            if (viewTools == null)
                viewTools = new EditorFavoritesViewTools();
            if (viewTreeList == null)
                viewTreeList = new EditorFavoritesViewTreeList();
            if (viewSearchList == null)
                viewSearchList = new EditorFavoritesViewSearchList();
        }

        private void OnGUI()
        {
            if (Event.current.type == EventType.Layout)
                return;
            
            styles.Initialize(GUI.skin);
            var rectTools = new Rect(0.0f, 0.0f, position.width, EditorGUIUtility.singleLineHeight);
            var rectList = new Rect(0.0f, rectTools.yMax, position.width, position.height - rectTools.height);
            
            viewTools.Initialize();
            viewTools.Draw(this, rectTools, styles);

            if (viewTools.IsSearching())
            {
                viewSearchList.Initialize();
                viewSearchList.Draw(this, rectList, styles);
            }
            else
            {
                viewTreeList.Initialize();
                viewTreeList.Draw(this, rectList, styles);
            }
            
            HandleDragAndDropEvents();
            HandleManipulatedEntry(styles);
            FavoritesGUIUtility.EndDraw();
        }

        private void HandleDragAndDropEvents()
        {
            Favorites favorites = FavoritesUtility.GetFavorites();
            
            if (Event.current.type == EventType.DragUpdated)
            {
                DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
            }
            else if (Event.current.type == EventType.DragPerform)
            {
                Favorites.Entry toParent = null;

                if (!viewTools.IsSearching())
                    toParent = FavoritesGUIUtility.hoverEntry;

                favorites.AddRangeByPath(DragAndDrop.paths, toParent);
            }
        }

        private static void HandleManipulatedEntry(Styles styles)
        {
            Favorites favorites = FavoritesUtility.GetFavorites();
            Favorites.Entry entry = FavoritesGUIUtility.manipulatingEntry;
            
            switch (FavoritesGUIUtility.manipulatingState)
            {
                case EManipulatingState.PerformedClick:
                    if (entry.isAsset)
                    {
                        Selection.activeObject = favorites.GetObject(entry);
                        EditorGUIUtility.PingObject(Selection.activeObject);
                    }
                    else
                    {
                        entry.expanded = !entry.expanded;
                        favorites.Save();
                    }
                    
                    FavoritesGUIUtility.ClearManipulating();
                    break;
                case EManipulatingState.PerformedDoubleClick:
                    if (entry.isAsset)
                        AssetDatabase.OpenAsset(favorites.GetObject(entry));
                    
                    FavoritesGUIUtility.ClearManipulating();
                    break;
                case EManipulatingState.BeginDrag:
                    Rect rect = FavoritesGUIUtility.manipulatingRect;
                    rect.position = Event.current.mousePosition - FavoritesGUIUtility.manipulatingMouseOffset;
                    FavoritesGUIUtility.DrawShadowEntry(rect, entry, styles);
                    break;
                case EManipulatingState.PerformedDrag:
                    favorites.Move(entry, FavoritesGUIUtility.hoverEntry);
                    FavoritesGUIUtility.ClearManipulating();
                    break;
                case EManipulatingState.PerformedContextClick:
                    FavoritesGUIUtility.ClearManipulating();
                    EditorFavoritesPopupWindowContextMenu.Show(Event.current.mousePosition, entry);
                    break;
                case EManipulatingState.EndClick:
                case EManipulatingState.EndDrag:
                case EManipulatingState.EndContextClick:
                    FavoritesGUIUtility.ClearManipulating();
                    break;
            }
        }
    }
}