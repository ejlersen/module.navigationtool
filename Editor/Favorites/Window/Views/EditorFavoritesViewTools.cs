﻿using System;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Favorite
{
    [Serializable]
    internal sealed class EditorFavoritesViewTools : AbstractEditorFavoritesView
    {
        public string searchStr;
        
        public override void Initialize()
        {
        }

        public override void Draw(EditorFavoritesWindow window, Rect rect, Styles styles)
        {
            float dim = rect.height;
            
            GUI.BeginGroup(rect, styles.toolbox);
            {
                var r0 = new Rect(0.0f, 0.0f, rect.width - dim, dim);
                var r1 = new Rect(rect.width - r0.height, 0.0f, dim, dim);

                searchStr = EditorGUI.TextField(r0, searchStr);

                if (GUI.Button(r1, "+", styles.buttonAddFolder))
                    EditorFavoritesPopupWindowAddFolder.Show(new Vector2(r1.xMax, dim), null);
            }
            GUI.EndGroup();
        }

        public bool IsSearching()
        {
            return !string.IsNullOrEmpty(searchStr);
        }
    }
}