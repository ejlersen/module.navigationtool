﻿using UnityEngine;

namespace Module.NavigationTool.Editor.Favorite
{
    internal abstract class AbstractEditorFavoritesView
    {
        public abstract void Initialize();
        public abstract void Draw(EditorFavoritesWindow window, Rect rect, Styles styles);
    }
}