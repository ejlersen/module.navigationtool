﻿using System;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Favorite
{
    [Serializable]
    internal sealed class EditorFavoritesViewTreeList : AbstractEditorFavoritesView
    {
        [SerializeField]
        private Vector2 scrollPosition;
        
        [NonSerialized]
        private Favorites favorites;

        public override void Initialize()
        {
            favorites = FavoritesUtility.GetFavorites();
        }

        public override void Draw(EditorFavoritesWindow window, Rect rect, Styles styles)
        {
            float entryHeight = EditorGUIUtility.singleLineHeight;
            float height = entryHeight * favorites.entries.Count;
            
            GUI.BeginGroup(rect);
            {
                var position = new Rect(0.0f, 0.0f, rect.width, rect.height);
                var viewRect = new Rect(0.0f, 0.0f, position.height > height ? position.width : position.width - 14.0f, height);
                var entryRect = new Rect(0.0f, 0.0f, viewRect.width, entryHeight);
            
                scrollPosition = GUI.BeginScrollView(position, scrollPosition, viewRect);
                {
                    for (var i = 0; i < favorites.entries.Count; i++)
                    {
                        Favorites.Entry e = favorites.entries[i];

                        if (e.indentLevel == 0)
                            DrawEntry(e, ref entryRect, entryHeight, styles);
                    }
                }
                GUI.EndScrollView();
            }
            GUI.EndGroup();
        }

        private void DrawEntry(Favorites.Entry entry, ref Rect entryRect, float entryHeight, Styles styles)
        {
            FavoritesGUIUtility.DrawEntry(entryRect, entry, styles);
            entryRect.y += entryHeight;

            if (entry.isAsset || !entry.expanded)
                return;

            for (var i = 0; i < entry.children.Count; i++)
            {
                DrawEntry(entry.children[i].entry, ref entryRect, entryHeight, styles);
            }
        }
    }
}