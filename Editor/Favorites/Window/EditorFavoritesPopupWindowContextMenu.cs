﻿using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Favorite
{
    internal sealed class EditorFavoritesPopupWindowContextMenu : PopupWindowContent
    {
        private readonly Favorites.Entry entry;
        private readonly Option[] options;
        private readonly Styles styles;

        public static void Show(Vector2 mousePosition, Favorites.Entry entry)
        {
            var rect = new Rect(mousePosition.x, mousePosition.y, 0.0f, 0.0f);
            PopupWindow.Show(rect, new EditorFavoritesPopupWindowContextMenu(entry));
        }

        private EditorFavoritesPopupWindowContextMenu(Favorites.Entry entry)
        {
            this.entry = entry;
            styles = new Styles();
            
            options = new[]
            {
                new Option("Add Folder", OnAddFolder, () => entry == null || !entry.isAsset),
                new Option("Rename Folder", OnRenameFolder, () =>  entry != null && !entry.isAsset),
                new Option("Remove", OnRemove, () => entry != null)
            };
        }
        
        public override void OnGUI(Rect rect)
        {
            styles.Initialize(GUI.skin);
            var entryRect = new Rect(0.0f, 0.0f, rect.width, EditorGUIUtility.singleLineHeight);

            for (var i = 0; i < options.Length; i++)
            {
                GUI.enabled = options[i].criteria.Invoke();
                
                if (GUI.Button(entryRect, options[i].name, styles.contextOption))
                    options[i].action.Invoke();

                entryRect.y += EditorGUIUtility.singleLineHeight;
            }
            
            GUI.enabled = true;
        }

        public override Vector2 GetWindowSize()
        {
            return new Vector2(200.0f, options.Length * EditorGUIUtility.singleLineHeight + 6.0f);
        }

        private void OnAddFolder()
        {
            editorWindow.Close();
            EditorFavoritesPopupWindowAddFolder.Show(new Vector2(0.0f, 0.0f), entry);
        }

        private void OnRenameFolder()
        {
            editorWindow.Close();
            EditorFavoritesPopupWindowRenameFolder.Show(new Vector2(0.0f, 0.0f), entry);
        }
        
        private void OnRemove()
        {
            Favorites favorites = FavoritesUtility.GetFavorites();
            favorites.Remove(entry);
            editorWindow.Close();
        }

        /// <summary>
        /// Class: Option in drop down menu
        /// </summary>
        private sealed class Option
        {
            public delegate void DoAction();
            public delegate bool DoActionCriteria();
            
            public readonly string name;
            public readonly DoAction action;
            public readonly DoActionCriteria criteria;

            public Option(string name, DoAction action, DoActionCriteria criteria = null)
            {
                this.name = name;
                this.action = action;
                this.criteria = criteria;
            }
        }
    }
}