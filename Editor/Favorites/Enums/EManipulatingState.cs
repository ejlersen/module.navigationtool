﻿namespace Module.NavigationTool.Editor.Favorite
{
    public enum EManipulatingState : byte
    {
        None,
        BeginClick,
        PerformedClick,
        PerformedDoubleClick,
        EndClick,
        BeginContextClick,
        PerformedContextClick,
        EndContextClick,
        BeginDrag,
        PerformedDrag,
        EndDrag
    }
}