﻿using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    public abstract class AbstractToolbarDrawer
    {
        public abstract bool Visible { get; }
        public abstract bool Enabled { get; }
        public abstract EToolbarPlacement Placement { get; }
        public abstract int Priority { get; }
        
        private Rect rect;
        protected Styles styles;

        public void Setup(Rect rect)
        {
            this.rect = rect;
        }

        public virtual void Update()
        {
        }

        public void OnGUI()
        {
            if (styles == null)
                styles = new Styles();
            
            styles.Initialize(GUI.skin);
            
            GUI.enabled = Enabled;
            Draw(rect);
            GUI.enabled = true;
        }

        protected abstract void Draw(Rect rect);
        public abstract float CalculateWidth();
    }
}