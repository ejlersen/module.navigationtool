﻿namespace Module.NavigationTool.Editor.Toolbar
{
    public interface IToolbarSettings
    {
        string Title { get; }

        void Initialize();
        void Draw();
    }
}