using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal static class ToolbarSettingsProvider
    {
        private static Styles STYLES;
        private static IToolbarSettings[] SETTINGS;

        private const string PREF_FOLDOUT_STATE = "ToolbarFoldout_{0}";
        
        [SettingsProvider]
        public static SettingsProvider GetProvider()
        {
            Initialize();
            var keywords = new List<string> { "Toolbar" };

            for (var i = 0; i < SETTINGS.Length; i++)
            {
                keywords.Add(SETTINGS[i].Title);
            }

            return new SettingsProvider("Module/Toolbar", SettingsScope.User)
            {
                label = "Toolbar",
                keywords = keywords.ToArray(),
                guiHandler = OnGui
            };
        }

        private static void Initialize()
        {
            if (STYLES == null)
                STYLES = new Styles();
            if (SETTINGS == null)
                SETTINGS = ToolbarSettingsUtility.GetAllUserSettings();
        }

        private static void OnGui(string searchContext)
        {
            Initialize();
            STYLES.Initialize(GUI.skin);

            EditorGUILayout.BeginVertical(STYLES.settingsGroup);
            {
                for (var i = 0; i < SETTINGS.Length; i++)
                {
                    IToolbarSettings settings = SETTINGS[i];

                    try
                    {
                        bool foldoutState = GetFoldoutState(settings);
                        bool newFoldoutState = EditorGUILayout.BeginFoldoutHeaderGroup(foldoutState, settings.Title, EditorStyles.foldoutHeader);

                        if (newFoldoutState)
                        {
                            EditorGUI.indentLevel++;
                            settings.Draw();
                            EditorGUI.indentLevel--;                            
                        }
                        
                        EditorGUILayout.EndFoldoutHeaderGroup();
                        EditorGUILayout.Space();
                        
                        if (newFoldoutState != foldoutState)
                            SetFoldoutState(settings, newFoldoutState);
                    }
                    catch (Exception e)
                    {
                        EditorGUILayout.LabelField(e.Message);
                    }
                }
            }
            EditorGUILayout.EndVertical();
        }

        private static bool GetFoldoutState(IToolbarSettings settings)
        {
            string key = string.Format(PREF_FOLDOUT_STATE, settings.GetType().FullName);
            return EditorPrefs.GetBool(key, true);
        }

        private static void SetFoldoutState(IToolbarSettings settings, bool foldout)
        {
            string key = string.Format(PREF_FOLDOUT_STATE, settings.GetType().FullName);
            EditorPrefs.SetBool(key, foldout);
        }
    }
}
