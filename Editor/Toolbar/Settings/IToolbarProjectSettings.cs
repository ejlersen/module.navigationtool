﻿namespace Module.NavigationTool.Editor.Toolbar
{
    public interface IToolbarProjectSettings
    {
        string Title { get; }
        bool IsSettingsDirty { get; }

        void Initialize(ToolbarProjectSettings projectSettings);
        void Draw();
        void SetSettingsValue();
    }
}