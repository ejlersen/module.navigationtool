﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal static class ToolbarProjectSettingsProvider
    {
        private static Styles STYLES;
        private static IToolbarProjectSettings[] SETTINGS;
        private static ToolbarProjectSettings PROJECT_SETTINGS;
        private const string PREF_FOLDOUT_STATE = "ToolbarFoldout_{0}";
        
        [SettingsProvider]
        public static SettingsProvider GetProvider()
        {
            Initialize();
            var keywords = new List<string> { "Toolbar" };
            
            for (var i = 0; i < SETTINGS.Length; i++)
            {
                keywords.Add(SETTINGS[i].Title);
            }
            
            return new SettingsProvider("Module/Toolbar", SettingsScope.Project)
            {
                label = "Toolbar",
                keywords = keywords.ToArray(),
                guiHandler = OnGui
            };
        }

        private static void Initialize()
        {
            if (STYLES == null)
                STYLES = new Styles();
            
            if (PROJECT_SETTINGS == null)
            {
                PROJECT_SETTINGS = new ToolbarProjectSettings();
                PROJECT_SETTINGS.Load();
            }
            
            if (SETTINGS == null)
                SETTINGS = ToolbarSettingsUtility.GetAllProjectSettings(PROJECT_SETTINGS);
        }
        
        private static void OnGui(string searchContext)
        {
            Initialize();
            STYLES.Initialize(GUI.skin);
            var isSettingsDirty = false;
        
            EditorGUILayout.BeginVertical(STYLES.settingsGroup);
            {
                for (var i = 0; i < SETTINGS.Length; i++)
                {
                    IToolbarProjectSettings settings = SETTINGS[i];
        
                    try
                    {
                        bool foldoutState = GetFoldoutState(settings);
                        bool newFoldoutState = EditorGUILayout.BeginFoldoutHeaderGroup(foldoutState, settings.Title, EditorStyles.foldoutHeader);
        
                        if (newFoldoutState)
                        {
                            EditorGUI.indentLevel++;
                            settings.Draw();
                            
                            if (settings.IsSettingsDirty)
                            {
                                isSettingsDirty = true;
                                settings.SetSettingsValue();
                            }

                            EditorGUI.indentLevel--;                            
                        }
                        
                        EditorGUILayout.EndFoldoutHeaderGroup();
                        EditorGUILayout.Space();
                        
                        if (newFoldoutState != foldoutState)
                            SetFoldoutState(settings, newFoldoutState);
                    }
                    catch (Exception e)
                    {
                        EditorGUILayout.LabelField(e.Message);
                    }
                }
                
                if (isSettingsDirty)
                    PROJECT_SETTINGS.Save();
            }
            EditorGUILayout.EndVertical();
        }
        
        private static bool GetFoldoutState(IToolbarProjectSettings settings)
        {
            string key = string.Format(PREF_FOLDOUT_STATE, settings.GetType().FullName);
            return EditorPrefs.GetBool(key, true);
        }
        
        private static void SetFoldoutState(IToolbarProjectSettings settings, bool foldout)
        {
            string key = string.Format(PREF_FOLDOUT_STATE, settings.GetType().FullName);
            EditorPrefs.SetBool(key, foldout);
        }
    }
}
