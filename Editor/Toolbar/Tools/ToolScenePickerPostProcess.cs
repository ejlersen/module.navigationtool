﻿using System.Linq;
using UnityEditor;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolScenePickerPostProcess : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            if (HasChange(deletedAssets) || HasChange(movedAssets) || HasChange(importedAssets))
                ToolScenePicker.SetAsDirty();
        }

        private static bool HasChange(string[] assets)
        {
            return assets.Any(s => s.EndsWith(".unity")) || 
                   assets.Any(s => s.EndsWith("EditorBuildSettings.asset"));
        }
    }
}