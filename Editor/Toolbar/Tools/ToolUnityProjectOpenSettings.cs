﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UTools = UnityEditor.Tools;

namespace Module.NavigationTool.Editor.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolUnityProjectOpenSettings : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarUnitySettings.IsProjectSettingsEnabled;
        public override bool Enabled => true;
        public override EToolbarPlacement Placement => EToolbarPlacement.Right;
        public override int Priority => (int)EToolbarPriority.Low;

        private static readonly GUIContent LABEL = new GUIContent(string.Empty, "Opens Preferences/Project Settings");
        private static readonly string[] OPTIONS = { "Preferences", "Project Settings" };

        protected override void Draw(Rect rect)
        {
            int temp = EditorGUI.Popup(rect, -1, OPTIONS, styles.popup);
            GUI.Label(rect, styles.iconSettings, styles.button);
            GUI.Label(rect, LABEL, styles.labelCenter);

            if (temp == -1)
                return;

            if (temp == 0)
                SettingsService.OpenUserPreferences("Module/Toolbar");
            else
                SettingsService.OpenProjectSettings("Module/Toolbar");
        }

        public override float CalculateWidth()
        {
            return 24.0f;
        }
    }
}