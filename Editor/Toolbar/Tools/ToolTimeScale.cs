﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UTools = UnityEditor.Tools;

namespace Module.NavigationTool.Editor.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolTimeScale : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarTimeSettings.IsTimeScaleEnabled;
        public override bool Enabled => true;
        public override EToolbarPlacement Placement => EToolbarPlacement.Left;
        public override int Priority => (int)EToolbarPriority.Low;

        private const string TOOLTIP = "Adjust Time.timeScale from [min;max] and snaps when value is approximately 1.0";
        private static readonly GUIContent LABEL_TIME_SCALE = new GUIContent("Time scale", TOOLTIP);
        private static readonly GUIContent LABEL_TIME_VALUE = new GUIContent("0.00", TOOLTIP);
        
        protected override void Draw(Rect rect)
        {
            float value = Time.timeScale;
            LABEL_TIME_VALUE.text = value.ToString("0.00");
            
            var r0 = new Rect(rect.x, rect.y - 2.0f, rect.width, rect.height * 0.5f);
            var r1 = new Rect(rect.x + 4.0f, rect.y + 2.0f, rect.width - 8.0f, rect.height * 0.5f);
            var r2 = new Rect(rect.x, r1.yMax, rect.width, rect.height * 0.5f);

            EditorGUI.LabelField(r0, LABEL_TIME_SCALE, styles.centeredMiniLabel);
            EditorGUI.LabelField(r2, LABEL_TIME_VALUE, styles.centeredMiniLabel);
            float temp = GUI.HorizontalSlider(r1, value, ToolbarTimeSettings.TimeScaleMinValue, ToolbarTimeSettings.TimeScaleMaxValue);
            temp = Mathf.Clamp(temp, ToolbarTimeSettings.TimeScaleMinValue, ToolbarTimeSettings.TimeScaleMaxValue);

            if (Mathf.Abs(temp - 1.0f) < 0.02f)
                temp = 1.0f;

            if (!Mathf.Approximately(value, temp))
                Time.timeScale = temp;
        }

        public override float CalculateWidth()
        {
            return 100.0f;
        }
    }
}