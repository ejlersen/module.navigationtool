﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UTools = UnityEditor.Tools;

namespace Module.NavigationTool.Editor.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolTargetFrameRate : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarTargetFrameRateSettings.IsTargetFrameRateEnabled;
        public override bool Enabled => true;
        public override EToolbarPlacement Placement => EToolbarPlacement.Left;
        public override int Priority => (int)EToolbarPriority.Low;

        private const string TOOLTIP = "Adjust Application.targetFrameRate from [min;max] target frame rate";
        private static readonly GUIContent LABEL_TARGET_FRAME_RATE = new GUIContent("Target frame rate", TOOLTIP);
        private static readonly GUIContent LABEL_TARGET_FRAME_RATE_VALUE = new GUIContent("0", TOOLTIP);
        
        protected override void Draw(Rect rect)
        {
            int value = Application.targetFrameRate;
            LABEL_TARGET_FRAME_RATE_VALUE.text = value.ToString("0");
            
            var r0 = new Rect(rect.x, rect.y - 2.0f, rect.width, rect.height * 0.5f);
            var r1 = new Rect(rect.x + 4.0f, rect.y + 2.0f, rect.width - 8.0f, rect.height * 0.5f);
            var r2 = new Rect(rect.x, r1.yMax, rect.width, rect.height * 0.5f);

            EditorGUI.LabelField(r0, LABEL_TARGET_FRAME_RATE, styles.centeredMiniLabel);
            EditorGUI.LabelField(r2, LABEL_TARGET_FRAME_RATE_VALUE, styles.centeredMiniLabel);
            
            float temp = GUI.HorizontalSlider(r1, value, ToolbarTargetFrameRateSettings.TargetFrameRateMinValue, ToolbarTargetFrameRateSettings.TargetFrameRateMaxValue);
            int iTemp = Mathf.RoundToInt(temp);
            iTemp = Mathf.Clamp(iTemp, ToolbarTargetFrameRateSettings.TargetFrameRateMinValue, ToolbarTargetFrameRateSettings.TargetFrameRateMaxValue);
            
            if (value != iTemp)
                Application.targetFrameRate = iTemp;
        }

        public override float CalculateWidth()
        {
            return 100.0f;
        }
    }
}