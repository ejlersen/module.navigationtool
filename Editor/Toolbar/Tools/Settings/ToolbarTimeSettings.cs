﻿using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarTimeSettings : IToolbarSettings
    {
        public string Title => "Time";
        
        private const string PREF_IS_TIME_SCALE_ENABLED = "ToolbarSettings.IsTimeScaleEnabled";
        private const string PREF_TIME_SCALE_MIN = "ToolbarSettings.TimeScaleMin";
        private const string PREF_TIME_SCALE_MAX = "ToolbarSettings.TimeScaleMax";
        
        public static bool IsTimeScaleEnabled
        {
            get => EditorPrefs.GetBool(PREF_IS_TIME_SCALE_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_IS_TIME_SCALE_ENABLED, value);
        }

        public static float TimeScaleMinValue
        {
            get => EditorPrefs.GetFloat(PREF_TIME_SCALE_MIN, 0.0f);
            set => EditorPrefs.SetFloat(PREF_TIME_SCALE_MIN, value);
        }

        public static float TimeScaleMaxValue
        {
            get => EditorPrefs.GetFloat(PREF_TIME_SCALE_MAX, 1.0f);
            set => EditorPrefs.SetFloat(PREF_TIME_SCALE_MAX, value);
        }

        public void Initialize()
        {
        }

        public void Draw()
        {
            IsTimeScaleEnabled = EditorGUILayout.Toggle("Enable Time slider", IsTimeScaleEnabled);

            GUI.enabled = IsTimeScaleEnabled;
            float timeScaleMinValue = EditorGUILayout.FloatField("Min Value", TimeScaleMinValue);
            float timeScaleMaxValue = EditorGUILayout.FloatField("Max Value", TimeScaleMaxValue);

            if (!Mathf.Approximately(timeScaleMinValue, TimeScaleMinValue))
            {
                if (timeScaleMinValue < 0.0f)
                    timeScaleMinValue = 0.0f;

                if (timeScaleMinValue > timeScaleMaxValue)
                    timeScaleMaxValue = timeScaleMinValue;
            }
            else if (!Mathf.Approximately(timeScaleMaxValue, TimeScaleMaxValue))
            {
                if (timeScaleMaxValue < 0.0f)
                    timeScaleMaxValue = 0.0f;

                if (timeScaleMaxValue < timeScaleMinValue)
                    timeScaleMinValue = timeScaleMaxValue;
            }
                
            TimeScaleMinValue = timeScaleMinValue;
            TimeScaleMaxValue = timeScaleMaxValue;
            GUI.enabled = true;
        }
    }
}