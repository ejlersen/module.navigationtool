﻿using UnityEditor;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarPlayerPrefsSettings : IToolbarSettings
    {
        public string Title => "Player Prefs";
        
        private const string PREF_PLAYER_PREFS_ENABLED = "ToolbarSettings.IsPlayerPrefsEnabled";
        
        public static bool IsPlayerPrefsEnabled
        {
            get => EditorPrefs.GetBool(PREF_PLAYER_PREFS_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_PLAYER_PREFS_ENABLED, value);
        }
        
        public void Initialize()
        {
        }

        public void Draw()
        {
            IsPlayerPrefsEnabled = EditorGUILayout.Toggle("Enable Player Prefs", IsPlayerPrefsEnabled);
        }
    }
}