﻿using UnityEditor;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarUnitySettings : IToolbarSettings
    {
        public string Title => "Unity Project";
        
        private const string PREF_PROJECT_SAVE_ENABLED = "ToolbarSettings.IsProjectSaveEnabled";
        private const string PREF_PROJECT_SETTINGS_ENABLED = "ToolbarSettings.IsProjectSettingsEnabled";
        
        public static bool IsProjectSaveEnabled
        {
            get => EditorPrefs.GetBool(PREF_PROJECT_SAVE_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_PROJECT_SAVE_ENABLED, value);
        }
        
        public static bool IsProjectSettingsEnabled
        {
            get => EditorPrefs.GetBool(PREF_PROJECT_SETTINGS_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_PROJECT_SETTINGS_ENABLED, value);
        }
        
        public void Initialize()
        {
        }

        public void Draw()
        {
            IsProjectSaveEnabled = EditorGUILayout.Toggle("Enable Save button", IsProjectSaveEnabled);
            IsProjectSettingsEnabled = EditorGUILayout.Toggle("Enable Project Settings button", IsProjectSettingsEnabled);
        }
    }
}