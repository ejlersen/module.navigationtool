﻿using UnityEditor;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarUiSettings : IToolbarSettings
    {
        public string Title => "UI";
        
        private const string PREF_IS_UI_ENABLED = "ToolbarSettings_IsUiEnabled";
        private const string PREF_IS_UI_LAYER_ENABLED = "ToolbarSettings_IsUiLayerEnabled";
        
        public static bool IsUiEnabled
        {
            get => EditorPrefs.GetBool(PREF_IS_UI_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_IS_UI_ENABLED, value);
        }

        public static bool IsUiLayerEnabled
        {
            get => EditorPrefs.GetBool(PREF_IS_UI_LAYER_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_IS_UI_LAYER_ENABLED, value);
        }

        public void Initialize()
        {
        }

        public void Draw()
        {
            IsUiEnabled = EditorGUILayout.Toggle("Enable Canvas picker", IsUiEnabled);
            IsUiLayerEnabled = EditorGUILayout.Toggle("Enable Layer toggle", IsUiLayerEnabled);
        }
    }
}