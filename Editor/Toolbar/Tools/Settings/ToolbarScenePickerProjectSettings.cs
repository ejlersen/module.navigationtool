﻿using System;
using System.Collections.Generic;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarScenePickerProjectSettings : IToolbarProjectSettings
    {
        public string Title => "Scene";
        public bool IsSettingsDirty { get; private set; }
        
        private StringReorderableListDrawer assetLabelList;
        private SceneGroupReorderableListDrawer sceneGroupList;

        private ToolbarProjectSettings projectSettings;
        private Settings settings;
        
        public void Initialize(ToolbarProjectSettings projectSettings)
        {
            this.projectSettings = projectSettings;
            settings = projectSettings.GetValueAs<Settings>();
            
            assetLabelList = new StringReorderableListDrawer(settings.labels, "Scene sorting by Asset label");
            assetLabelList.onChanged += ToolScenePicker.SetAsDirty;
            
            sceneGroupList = new SceneGroupReorderableListDrawer(settings.sceneGroups.groups, "Scene groups");
            sceneGroupList.onChanged += ToolScenePicker.SetAsDirty;
        }

        public void Draw()
        {
            assetLabelList.DoLayoutList();
            sceneGroupList.DoLayoutList();
            IsSettingsDirty = assetLabelList.IsDirty || sceneGroupList.IsDirty;
        }

        public void SetSettingsValue()
        {
            projectSettings.SetValue(settings);
        }

        [Serializable]
        public sealed class Settings
        {
            public List<string> labels = new();
            public SceneGroupArray sceneGroups = new();
        }
    }
}