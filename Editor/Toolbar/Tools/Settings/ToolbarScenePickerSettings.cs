﻿using UnityEditor;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarScenePickerSettings : IToolbarSettings
    {
        public string Title => "Scene";
        
        private const string PREF_IS_SCENE_ENABLED = "ToolbarSettings_IsSceneEnabled";
        private const string PREF_SCENE_PICKER_LOAD_SET_AS_ADDITIVE_KEY = "ToolbarSettings_ScenePickerLoadSetAsAdditive";
        
        public static bool IsSceneEnabled
        {
            get => EditorPrefs.GetBool(PREF_IS_SCENE_ENABLED, true);
            private set => EditorPrefs.SetBool(PREF_IS_SCENE_ENABLED, value);
        }
        
        public static bool IsScenePickerSetAsAdditive
        {
            get => EditorPrefs.GetBool(PREF_SCENE_PICKER_LOAD_SET_AS_ADDITIVE_KEY, false);
            set => EditorPrefs.SetBool(PREF_SCENE_PICKER_LOAD_SET_AS_ADDITIVE_KEY, value);
        }
        
        public void Initialize()
        {
        }

        public void Draw()
        {
            IsSceneEnabled = EditorGUILayout.Toggle("Enable Scene picker", IsSceneEnabled);
        }
    }
}