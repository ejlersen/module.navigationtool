﻿using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarTargetFrameRateSettings : IToolbarSettings
    {
        public string Title => "Target Frame Rate";
        
        private const string PREF_IS_TARGET_FRAME_RATE_ENABLED = "ToolbarSettings.IsTargetFrameRateEnabled";
        private const string PREF_TARGET_FRAME_RATE_MIN = "ToolbarSettings.TargetFrameRateMin";
        private const string PREF_TARGET_FRAME_RATE_MAX = "ToolbarSettings.TargetFrameRateMax";
        
        public static bool IsTargetFrameRateEnabled
        {
            get => EditorPrefs.GetBool(PREF_IS_TARGET_FRAME_RATE_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_IS_TARGET_FRAME_RATE_ENABLED, value);
        }

        public static int TargetFrameRateMinValue
        {
            get => EditorPrefs.GetInt(PREF_TARGET_FRAME_RATE_MIN, 10);
            set => EditorPrefs.SetInt(PREF_TARGET_FRAME_RATE_MIN, value);
        }

        public static int TargetFrameRateMaxValue
        {
            get => EditorPrefs.GetInt(PREF_TARGET_FRAME_RATE_MAX, 144);
            set => EditorPrefs.SetInt(PREF_TARGET_FRAME_RATE_MAX, value);
        }

        public void Initialize()
        {
        }

        public void Draw()
        {
            IsTargetFrameRateEnabled = EditorGUILayout.Toggle("Enable Frame Rate", IsTargetFrameRateEnabled);

            GUI.enabled = IsTargetFrameRateEnabled;
            int minValue = EditorGUILayout.IntField("Min", TargetFrameRateMinValue);
            int maxValue = EditorGUILayout.IntField("Max", TargetFrameRateMaxValue);

            if (minValue != TargetFrameRateMinValue)
            {
                if (minValue < 1)
                    minValue = 1;

                if (minValue > maxValue)
                    maxValue = minValue;
            }
            else if (maxValue != TargetFrameRateMaxValue)
            {
                if (maxValue < 1)
                    maxValue = 1;

                if (maxValue < minValue)
                    minValue = maxValue;
            }
                
            TargetFrameRateMinValue = minValue;
            TargetFrameRateMaxValue = maxValue;
            GUI.enabled = true;
        }
    }
}