﻿using UnityEditor;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarBuildSettings : IToolbarSettings
    {
        public string Title => "Build";
        
        private const string PREF_IS_BUILD_ENABLED = "ToolbarSettings.IsBuildEnabled";
        private const string PREF_IS_BUILD_AND_RUN_ENABLED = "ToolbarSettings.IsBuildAndRunEnabled";

        public static bool IsBuildEnabled
        {
            get => EditorPrefs.GetBool(PREF_IS_BUILD_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_IS_BUILD_ENABLED, value);
        }

        public static bool IsBuildAndRunEnabled
        {
            get => EditorPrefs.GetBool(PREF_IS_BUILD_AND_RUN_ENABLED, true);
            set => EditorPrefs.SetBool(PREF_IS_BUILD_AND_RUN_ENABLED, value);
        }

        public void Initialize()
        {
        }

        public void Draw()
        {
            IsBuildEnabled = EditorGUILayout.Toggle("Enable Build Target", IsBuildEnabled);
        }
    }
}