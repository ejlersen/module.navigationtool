﻿using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace Module.NavigationTool.Editor.Toolbar
{
    [InitializeOnLoad]
    internal static class ToolUICanvasPickerPostProcess
    {
        static ToolUICanvasPickerPostProcess()
        {
            EditorSceneManager.sceneSaved += OnSceneSaved;
            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneClosed += OnSceneClosed;

            PrefabStage.prefabStageOpened += OnPrefabStageOpened;
            PrefabStage.prefabStageClosing += OnPrefabStageClosing;
        }

        private static void OnSceneSaved(Scene scene)
        {
            ToolUICanvasPicker.SetAsDirty();
        }

        private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            ToolUICanvasPicker.SetAsDirty();
        }

        private static void OnSceneClosed(Scene scene)
        {
            ToolUICanvasPicker.SetAsDirty();
        }

        private static void OnPrefabStageOpened(PrefabStage stage)
        {
            ToolUICanvasPicker.SetAsDirty();
        }

        private static void OnPrefabStageClosing(PrefabStage stage)
        {
            ToolUICanvasPicker.SetAsDirty();
        }
    }
}