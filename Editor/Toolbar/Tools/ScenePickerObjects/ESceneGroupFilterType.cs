﻿namespace Module.NavigationTool.Editor.Toolbar
{
    public enum ESceneGroupFilterType
    {
        AssetLabels,
        NameContains,
        PathContains
    }
}