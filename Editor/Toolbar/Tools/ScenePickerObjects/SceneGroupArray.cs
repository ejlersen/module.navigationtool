﻿using System;
using System.Collections.Generic;

namespace Module.NavigationTool.Editor.Toolbar
{
    [Serializable]
    public sealed class SceneGroupArray
    {
        public List<SceneGroup> groups = new();
        
        public int Count => groups.Count;
        public SceneGroup this[int index] => groups[index];
    }
}