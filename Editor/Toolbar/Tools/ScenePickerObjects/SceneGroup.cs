﻿using System;
using System.Collections.Generic;

namespace Module.NavigationTool.Editor.Toolbar
{
    [Serializable]
    public sealed class SceneGroup
    {
        public string name;
        public ESceneGroupFilterType filterType;
        public string optionalMainScenePath;
        public List<string> filters = new();
    }
}