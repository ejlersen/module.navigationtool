﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UTools = UnityEditor.Tools;

namespace Module.NavigationTool.Editor.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolUnityProjectSave : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarUnitySettings.IsProjectSaveEnabled;
        public override bool Enabled => true;
        public override EToolbarPlacement Placement => EToolbarPlacement.Right;
        public override int Priority => (int)EToolbarPriority.Low;

        private static readonly GUIContent LABEL = new GUIContent(string.Empty, "Saves all changed assets");

        protected override void Draw(Rect rect)
        {
            bool isButtonPressed = GUI.Button(rect, styles.iconProject, styles.button);
            GUI.Label(rect, LABEL, styles.labelCenter);
            
            if (isButtonPressed)
                AssetDatabase.SaveAssets();
        }

        public override float CalculateWidth()
        {
            return 24.0f;
        }
    }
}