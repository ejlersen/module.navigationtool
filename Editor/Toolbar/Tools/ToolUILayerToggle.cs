﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UTools = UnityEditor.Tools;

namespace Module.NavigationTool.Editor.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolUILayerToggle : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarUiSettings.IsUiLayerEnabled;
        public override bool Enabled => true;
        public override EToolbarPlacement Placement => EToolbarPlacement.Left;
        public override int Priority => (int)EToolbarPriority.Medium - 1;

        private static readonly GUIContent LABEL = new GUIContent("UI", "Toggles UI visible layer in SceneView");

        protected override void Draw(Rect rect)
        {
            int layer = 1 << LayerMask.NameToLayer("UI");
            bool value = (UTools.visibleLayers & layer) != 0;
            
            bool temp = EditorGUI.Toggle(rect, value, styles.button);
            GUI.Label(rect, LABEL, styles.labelCenter);

            if (temp == value)
                return;
            
            if (temp)
            {
                UTools.visibleLayers |= layer;
                SceneView.RepaintAll();
            }
            else
            {
                UTools.visibleLayers &= ~layer;
                SceneView.RepaintAll();
            }
        }

        public override float CalculateWidth()
        {
            return 30.0f;
        }
    }
}