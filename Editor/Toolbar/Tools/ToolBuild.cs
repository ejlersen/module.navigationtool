﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UTools = UnityEditor.Tools;

namespace Module.NavigationTool.Editor.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolBuild : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarBuildSettings.IsBuildEnabled;
        public override bool Enabled => !Application.isPlaying;
        public override EToolbarPlacement Placement => EToolbarPlacement.Left;
        public override int Priority => (int)EToolbarPriority.Low;

        private static bool IS_DIRTY = true;
        private static GUIContent[] BUTTON_LIST = new GUIContent[0];
        private static GUIContent[] CONTENT_LIST = new GUIContent[0];
        private static GUIContent DROPDOWN = new GUIContent();
        
        private static void Initialize()
        {
            if (!IS_DIRTY)
                return;

            const string TOOLTIP = "Build project with current EditorBuildSettings and target platform";
            const string TOOLTIP_RUN = "Build & Run project with current EditorBuildSettings and target platform";
            const string TOOLTIP_PLATFORM = "Select build or build & run options";

            BUTTON_LIST = new[]
            {
                new GUIContent("Build", TOOLTIP),
                new GUIContent("Build & Run", TOOLTIP_RUN)
            };
            
            CONTENT_LIST = new[]
            {
                new GUIContent("Build"),
                new GUIContent("Build and Run")
            };

            GUIContent iconContent = EditorGUIUtility.IconContent("d_icon dropdown");
            DROPDOWN = new GUIContent(iconContent.image, TOOLTIP_PLATFORM);
            
            IS_DIRTY = false;
        }
        
        public override void Update()
        {
            Initialize();
        }
        
        protected override void Draw(Rect rect)
        {
            var rect0 = new Rect(rect.x, rect.y, rect.width - 16.0f, rect.height);
            var rect1 = new Rect(rect0.xMax, rect.y, 16.0f, rect.height);
            
            int currentSelected = ToolbarBuildSettings.IsBuildAndRunEnabled ? 1 : 0;
            
            if (GUI.Button(rect0, BUTTON_LIST[currentSelected], styles.buttonNoPadding))
            {
                BuildPlayerOptions options = BuildPlayerWindow.DefaultBuildMethods.GetBuildPlayerOptions(new BuildPlayerOptions());
                options.options |= BuildOptions.AutoRunPlayer;
                BuildPlayerWindow.DefaultBuildMethods.BuildPlayer(options);
            }

            if (GUI.Button(rect1, DROPDOWN, styles.buttonNoPadding))
            {
                // Note: Do not discard the parameters: userData and options, since they throw an error in 2019, if there are two of them
                EditorUtility.DisplayCustomMenu(rect, CONTENT_LIST, currentSelected, (userData, options, selected) =>
                {
                    if (selected != -1)
                        ToolbarBuildSettings.IsBuildAndRunEnabled = selected == 1;
                }, null);
            }
        }
        
        public override float CalculateWidth()
        {
            return 100.0f;
        }
    }
}