﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolPlayerPrefs : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarPlayerPrefsSettings.IsPlayerPrefsEnabled;
        public override bool Enabled => true;
        public override EToolbarPlacement Placement => EToolbarPlacement.Right;
        public override int Priority => (int)EToolbarPriority.Low;

        private static readonly GUIContent LABEL_DELETE = new GUIContent(string.Empty, "Deletes all PlayerPrefs entries");

        protected override void Draw(Rect rect)
        {
            bool isDeleteButtonPressed = GUI.Button(rect, styles.iconDisconnect, styles.button);
            GUI.Label(rect, LABEL_DELETE, styles.labelCenter);

            if (!isDeleteButtonPressed)
                return;
            if (!EditorUtility.DisplayDialog("Delete PlayerPrefs", "Are you sure you want to delete all PlayerPrefs data?", "Yes", "No"))
                return;

            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }

        public override float CalculateWidth()
        {
            return 24.0f;
        }
    }
}