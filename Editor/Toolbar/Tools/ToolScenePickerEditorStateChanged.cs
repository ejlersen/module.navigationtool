﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace Module.NavigationTool.Editor.Toolbar
{
    [InitializeOnLoad]
    internal static class ToolScenePickerEditorStateChanged
    {
        static ToolScenePickerEditorStateChanged()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
            SceneManager.activeSceneChanged += OnActiveSceneChanged;
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
            EditorSceneManager.newSceneCreated += OnNewSceneCreated;
            EditorSceneManager.activeSceneChangedInEditMode += OnActiveSceneChangedInEditMode;
            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneClosed += OnSceneClosed;
        }

        private static void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            ToolScenePicker.SetAsDirty();
        }

        private static void OnActiveSceneChanged(Scene current, Scene next)
        {
            ToolScenePicker.SetAsDirty();
        }
        
        private static void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            ToolScenePicker.SetAsDirty();
        }

        private static void OnSceneUnloaded(Scene arg0)
        {
            ToolScenePicker.SetAsDirty();
        }

        private static void OnActiveSceneChangedInEditMode(Scene from, Scene to)
        {
            ToolScenePicker.SetAsDirty();
        }

        private static void OnNewSceneCreated(Scene scene, NewSceneSetup setup, NewSceneMode mode)
        {
            ToolScenePicker.SetAsDirty();
        }
        
        private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            ToolScenePicker.SetAsDirty();
        }

        private static void OnSceneClosed(Scene scene)
        {
            ToolScenePicker.SetAsDirty();
        }
    }
}