﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class StringReorderableListDrawer : AbstractReorderableListDrawer<string>
    {
        public StringReorderableListDrawer(List<string> list, string header)
            : base(list, header)
        {
        }
        
        protected override void OnDrawElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            rect.height -= 2;
            rect.y += 1;
            
            string text = list[index];
            string temp = EditorGUI.TextField(rect, text);

            if (string.Equals(text, temp, StringComparison.Ordinal))
                return;

            list[index] = temp;
            InvokeChanged();
        }

        protected override void OnAddElementToList()
        {
            list.Add(string.Empty);
        }
    }
}