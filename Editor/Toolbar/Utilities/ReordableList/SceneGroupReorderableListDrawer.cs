﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class SceneGroupReorderableListDrawer : AbstractReorderableListDrawer<SceneGroup>
    {
        private readonly List<StringReorderableListDrawer> elements = new List<StringReorderableListDrawer>();
        
        public SceneGroupReorderableListDrawer(List<SceneGroup> list, string header)
            : base(list, header)
        {
            for (var i = 0; i < list.Count; i++)
            {
                elements.Add(new StringReorderableListDrawer(list[i].filters, "Filters"));
            }
        }
        
        protected override void OnDrawElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            rect.height -= 2;
            rect.y += 1;
             
            SceneGroup group = list[index];
            var rectName = new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight);
            var rectType = new Rect(rect.x, rectName.yMax + 2f, rect.width, EditorGUIUtility.singleLineHeight);
            var rectOptional = new Rect(rect.x, rectType.yMax + 2f, rect.width - 80f, EditorGUIUtility.singleLineHeight);
            var rectOptionalBtn = new Rect(rectOptional.xMax, rectType.yMax + 2f, 80f, EditorGUIUtility.singleLineHeight);
            var rectLabels = new Rect(rect.x, rectOptional.yMax + 2f, rect.width, rect.height - (rectName.y - rectOptional.yMax) - 4f);
            
            EditorGUI.BeginChangeCheck();
            group.name = EditorGUI.TextField(rectName, "Name", group.name);
            group.optionalMainScenePath = EditorGUI.TextField(rectOptional, "Optional Main Scene Path", group.optionalMainScenePath);

            if (GUI.Button(rectOptionalBtn, "Find"))
            {
                string mainScenePath = EditorUtility.OpenFilePanel("Scene", "Assets", "unity");
                
                if (!string.IsNullOrEmpty(mainScenePath))
                {
                    mainScenePath = mainScenePath.Substring(Application.dataPath.Length - 6);
                    group.optionalMainScenePath = mainScenePath;
                }
            }

            group.filterType = (ESceneGroupFilterType)EditorGUI.EnumPopup(rectType, "Filter Type", group.filterType);
            elements[index].DoList(rectLabels);
            
            if (EditorGUI.EndChangeCheck())
                InvokeChanged();
        }

        protected override void OnAddElementToList()
        {
            var filters = new List<string>();
            
            list.Add(new SceneGroup
            {
                name = "New Scene Group",
                filters = filters
            });
            
            elements.Add(new StringReorderableListDrawer(filters, "Filters"));
        }

        protected override void OnRemovedElementFromList(int index)
        {
            elements.RemoveAt(index);
        }

        protected override float OnElementHeight(int index)
        {
            int labelCount = list[index].filters.Count;
            
            return base.OnElementHeight(index) * 3f
                   + Mathf.Max(0, labelCount - 1) * (EditorGUIUtility.singleLineHeight + 2f)
                   + 78f;
        }
    }
}