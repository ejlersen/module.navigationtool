﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal abstract class AbstractReorderableListDrawer<T>
    {
        public readonly List<T> list;
        
        private readonly ReorderableList reorderableList;
        public event Action onChanged;
        
        public bool IsDirty { get; private set; }

        protected AbstractReorderableListDrawer(List<T> list, string header)
        {
            this.list = list;
                
            reorderableList = new ReorderableList(this.list, typeof(T), true, true, true, true);
            reorderableList.drawElementCallback += (rect, index, isActive, isFocused) =>
            {
                EditorGUI.BeginChangeCheck();
                OnDrawElement(rect, index, isActive, isFocused);

                if (EditorGUI.EndChangeCheck())
                    IsDirty = true;
            };
            reorderableList.drawHeaderCallback += rect => EditorGUI.LabelField(rect, header);
            reorderableList.onAddCallback += _ =>
            {
                OnAddElementToList();
                InvokeChanged();
            };
            reorderableList.onRemoveCallback += rl =>
            {
                this.list.RemoveAt(rl.index);
                OnRemovedElementFromList(rl.index);
                InvokeChanged();
            };
            reorderableList.onReorderCallback += _ => InvokeChanged();;
            reorderableList.elementHeightCallback += OnElementHeight;
        }

        public void DoList(Rect rect)
        {
            IsDirty = false;
            reorderableList.DoList(rect);
        }

        public void DoLayoutList()
        {
            IsDirty = false;
            reorderableList.DoLayoutList();
        }

        protected abstract void OnDrawElement(Rect rect, int index, bool isActive, bool isFocused);
        protected abstract void OnAddElementToList();

        protected virtual void OnRemovedElementFromList(int index)
        {
        }

        protected virtual float OnElementHeight(int index)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        protected void InvokeChanged()
        {
            IsDirty = true;
            onChanged?.Invoke();
        }
    }
}