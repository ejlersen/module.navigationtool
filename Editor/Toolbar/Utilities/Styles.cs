﻿using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Toolbar
{
    public sealed class Styles
    {
        public GUIStyle popup;
        public GUIStyle button;
        public GUIStyle buttonNoPadding;
        public GUIStyle slider;
        public GUIStyle label;
        public GUIStyle labelCenter;
        public GUIStyle settingsGroup;
        public GUIStyle centeredMiniLabel;

        public GUIContent iconPlusSmall;
#if !UNITY_6000_0_OR_NEWER
        public GUIContent iconSceneAdditive;
        public GUIContent iconSceneSingle;
#endif
        public GUIContent iconDisconnect;
        public GUIContent iconProject;
        public GUIContent iconSettings;
        
        private GUISkin skin;

        public void Initialize(GUISkin skin)
        {
            if (this.skin == skin)
                return;

            this.skin = skin;
            popup = new GUIStyle(skin.FindStyle("ToolbarPopup"));
            button = new GUIStyle(skin.FindStyle("toolbarbutton"));
            slider = new GUIStyle(skin.FindStyle("ToolbarSlider"));
            label = new GUIStyle(skin.FindStyle("ToolbarLabel"));

            if (EditorGUIUtility.isProSkin)
            {
                centeredMiniLabel = EditorStyles.centeredGreyMiniLabel;
            }
            else
            {
                centeredMiniLabel = new GUIStyle(EditorStyles.centeredGreyMiniLabel);
                centeredMiniLabel.normal.textColor = label.normal.textColor;
            }
            
            buttonNoPadding = new GUIStyle(skin.FindStyle("toolbarbutton"))
            {
                padding = new RectOffset()
            };
            
            labelCenter = new GUIStyle(skin.FindStyle("ToolbarLabel"))
            {
                alignment = TextAnchor.MiddleCenter
            };

            settingsGroup = new GUIStyle
            {
                margin = new RectOffset(8, 0, 8, 0)
            };

            if (EditorGUIUtility.isProSkin)
            {
                iconPlusSmall = EditorGUIUtility.IconContent("d_CreateAddNew");
                
#if !UNITY_6000_0_OR_NEWER
                iconSceneAdditive = EditorGUIUtility.IconContent("d_winbtn_win_restore_h");
                iconSceneSingle = EditorGUIUtility.IconContent("d_winbtn_win_max_h");
#endif
            }
            else
            {
                iconPlusSmall = EditorGUIUtility.IconContent("CreateAddNew");
                
#if !UNITY_6000_0_OR_NEWER
                iconSceneAdditive = EditorGUIUtility.IconContent("winbtn_win_restore_h");
                iconSceneSingle = EditorGUIUtility.IconContent("winbtn_win_max_h");
#endif
            }
            
            iconDisconnect = EditorGUIUtility.IconContent("d_CacheServerDisconnected");
            iconProject = EditorGUIUtility.IconContent("Project");
            iconSettings = new GUIContent(EditorGUIUtility.IconContent("Settings"));
        }
    }
}