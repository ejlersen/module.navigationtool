﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal static class ToolbarSettingsUtility
    {
        public static IToolbarSettings[] GetAllUserSettings()
        {
            var list = new List<IToolbarSettings>(8);

            try
            {
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                Type iType = typeof(IToolbarSettings);
                
                for (var i = 0; i < assemblies.Length; i++)
                {
                    Assembly assembly = assemblies[i];
                    Type[] types = assembly.GetTypes();

                    for (var j = 0; j < types.Length; j++)
                    {
                        Type type = types[j];

                        if (type.IsInterface || type.IsAbstract || !iType.IsAssignableFrom(type))
                            continue;

                        var toolbar = (IToolbarSettings)FormatterServices.GetUninitializedObject(type);
                        toolbar.Initialize();
                        list.Add(toolbar);
                    }
                }
                
                list.Sort((s0, s1) => string.Compare(s0.Title, s1.Title, StringComparison.Ordinal));
            }
            catch (Exception)
            {
                // Fail silently
            }

            return list.ToArray();
        }
        
        public static IToolbarProjectSettings[] GetAllProjectSettings(ToolbarProjectSettings settings)
        {
            var list = new List<IToolbarProjectSettings>(8);

            try
            {
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                Type iType = typeof(IToolbarProjectSettings);
                
                for (var i = 0; i < assemblies.Length; i++)
                {
                    Assembly assembly = assemblies[i];
                    Type[] types = assembly.GetTypes();

                    for (var j = 0; j < types.Length; j++)
                    {
                        Type type = types[j];

                        if (type.IsInterface || type.IsAbstract || !iType.IsAssignableFrom(type))
                            continue;

                        var toolbar = (IToolbarProjectSettings)FormatterServices.GetUninitializedObject(type);
                        toolbar.Initialize(settings);
                        list.Add(toolbar);
                    }
                }
                
                list.Sort((s0, s1) => string.Compare(s0.Title, s1.Title, StringComparison.Ordinal));
            }
            catch (Exception)
            {
                // Fail silently
            }

            return list.ToArray();
        }
    }
}