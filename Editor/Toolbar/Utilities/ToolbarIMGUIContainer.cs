﻿using System;
using UnityEngine.UIElements;

namespace Module.NavigationTool.Editor.Toolbar
{
    internal sealed class ToolbarIMGUIContainer : IMGUIContainer
    {
        public int Priority { get; }

        public ToolbarIMGUIContainer(Action onGuiHandler, int priority)
            : base(onGuiHandler)
        {
            Priority = priority;
        }
    }
}