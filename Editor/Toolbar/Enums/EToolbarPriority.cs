﻿namespace Module.NavigationTool.Editor.Toolbar
{
    public enum EToolbarPriority
    {
        Low,
        Medium = 1000,
        High   = 2000
    }
}