﻿namespace Module.NavigationTool.Editor.Toolbar
{
    public enum EToolbarPlacement : byte
    {
        Left,
        Right
    }
}