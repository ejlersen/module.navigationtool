﻿using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.Utilities
{
    internal static class EditorProjectPrefs
    {
        private static string PRODUCT_ID;

        public static void SetInt(string key, int value)
        {
            EditorPrefs.SetInt(GetProjectKey(key), value);
        }

        public static void SetFloat(string key, float value)
        {
            EditorPrefs.SetFloat(GetProjectKey(key), value);
        }

        public static void SetString(string key, string value)
        {
            EditorPrefs.SetString(GetProjectKey(key), value);
        }

        public static void SetBool(string key, bool value)
        {
            EditorPrefs.SetBool(GetProjectKey(key), value);
        }

        public static int GetInt(string key, int defaultValue = 0)
        {
            return EditorPrefs.GetInt(GetProjectKey(key), defaultValue);
        }

        public static float GetFloat(string key, float defaultValue = 0.0f)
        {
            return EditorPrefs.GetFloat(GetProjectKey(key), defaultValue);
        }
        
        public static string GetString(string key, string defaultValue = "")
        {
            return EditorPrefs.GetString(GetProjectKey(key), defaultValue);
        }
        
        public static bool GetBool(string key, bool defaultValue = false)
        {
            return EditorPrefs.GetBool(GetProjectKey(key), defaultValue);
        }
        
        public static bool HasKey(string key)
        {
            return EditorPrefs.HasKey(GetProjectKey(key));
        }
        
        public static void DeleteKey(string key)
        {
            EditorPrefs.DeleteKey(GetProjectKey(key));
        }
        
        private static string GetProjectKey(string key)
        {
            if (string.IsNullOrEmpty(PRODUCT_ID))
                PRODUCT_ID = ConstructProductIdFromPath();

            return $"{PRODUCT_ID}_{key}";
        }
        
        private static string ConstructProductIdFromPath()
        {
            var md5 = MD5.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(Application.dataPath);
            byte[] data = md5.ComputeHash(bytes);
            var sb = new StringBuilder();
            
            for (var i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }

            return sb.ToString();
        }
    }
}