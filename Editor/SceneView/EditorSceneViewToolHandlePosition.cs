﻿#if UNITY_2021_1_OR_NEWER
using UnityEditor;
using UnityEditor.Toolbars;
using UnityEngine;

namespace Module.NavigationTool.Editor.SceneViewToolbar
{
    [EditorToolbarElement("SceneView/Custom/HandlePosition", typeof(SceneView))]
    internal sealed class EditorSceneViewToolHandlePosition : EditorToolbarButton
    {
        public EditorSceneViewToolHandlePosition()
        {
            tooltip = "Toggle Tool Handle Position\nThe tool handle is placed at the active object's pivot point.";
            clicked += OnClicked;
            
            RefreshIcon();
            Tools.pivotModeChanged += OnPivotModeChanged;
        }

        ~EditorSceneViewToolHandlePosition()
        {
            Tools.pivotModeChanged -= OnPivotModeChanged;
        }

        private void RefreshIcon()
        {
            if (Tools.pivotMode == PivotMode.Center)
                icon = EditorGUIUtility.IconContent("d_ToolHandleCenter").image as Texture2D;
            else
                icon = EditorGUIUtility.IconContent("d_ToolHandlePivot").image as Texture2D;
        }

        private void OnClicked()
        {
            Tools.pivotMode = Tools.pivotMode == PivotMode.Center ? PivotMode.Pivot : PivotMode.Center;
        }

        private void OnPivotModeChanged()
        {
            RefreshIcon();
        }
    }
}
#endif
