﻿#if UNITY_2021_1_OR_NEWER
using UnityEditor;
using UnityEditor.Toolbars;
using UnityEngine;

namespace Module.NavigationTool.Editor.SceneViewToolbar
{
    [EditorToolbarElement("SceneView/Custom/HandleRotation", typeof(SceneView))]
    internal sealed class EditorSceneViewToolHandleRotation : EditorToolbarButton
    {
        public EditorSceneViewToolHandleRotation()
        {
            tooltip = "Toggle Tool Handle Rotation\nTool handles are in the active object's rotation.";
            clicked += OnClicked;
            
            RefreshIcon();
            Tools.pivotRotationChanged += OnPivotRotationChanged;
        }

        ~EditorSceneViewToolHandleRotation()
        {
            Tools.pivotRotationChanged -= OnPivotRotationChanged;
        }

        private void RefreshIcon()
        {
            if (Tools.pivotRotation == PivotRotation.Global)
                icon = EditorGUIUtility.IconContent("d_ToolHandleGlobal").image as Texture2D;
            else
                icon = EditorGUIUtility.IconContent("d_ToolHandleLocal").image as Texture2D;
        }

        private void OnClicked()
        {
            Tools.pivotRotation = Tools.pivotRotation == PivotRotation.Local ? PivotRotation.Global : PivotRotation.Local;
        }

        private void OnPivotRotationChanged()
        {
            RefreshIcon();
        }
    }
}
#endif
