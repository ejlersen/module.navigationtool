﻿#if UNITY_2021_1_OR_NEWER
using UnityEditor;
using UnityEditor.Overlays;

namespace Module.NavigationTool.Editor.SceneViewToolbar
{
    [Overlay(typeof(SceneView), "unity-custom-tool-handle-utility", "Custom/Tool Settings", true)]
    internal sealed class EditorSceneViewToolHandleOverlay : ToolbarOverlay
    {
        public EditorSceneViewToolHandleOverlay()
            : base("SceneView/Custom/HandlePosition", "SceneView/Custom/HandleRotation")
        {
        }
    }
}
#endif
