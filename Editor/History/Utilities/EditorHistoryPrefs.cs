﻿using UnityEditor;

namespace Module.NavigationTool.Editor.History
{
    internal static class EditorHistoryPrefs
    {
        private const string PREF_IS_SET_AS_ACTIVE_OBJECT_ENABLED = "EditorHistoryWindow.IsSetAsActiveObjectEnabled";
        
        public static bool IsSetAsActiveObjectEnabled
        {
            get => EditorPrefs.GetBool(PREF_IS_SET_AS_ACTIVE_OBJECT_ENABLED, true);
            set => EditorPrefs.SetBool(PREF_IS_SET_AS_ACTIVE_OBJECT_ENABLED, value);
        }
    }
}