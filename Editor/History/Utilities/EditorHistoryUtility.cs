﻿using System.Collections.Generic;

namespace Module.NavigationTool.Editor.History
{
    internal static class EditorHistoryUtility
    {
        private static EditorHistoryList EDITOR_HISTORY_LIST;

        static EditorHistoryUtility()
        {
            EDITOR_HISTORY_LIST = null;
        }
        
        public static EditorHistoryList GetHistoryList()
        {
            return EDITOR_HISTORY_LIST ?? (EDITOR_HISTORY_LIST = new EditorHistoryList());
        }

        public static bool IsLoaded()
        {
            return EDITOR_HISTORY_LIST != null;
        }

        public static void RefreshAll()
        {
            if (EDITOR_HISTORY_LIST == null)
                return;

            List<EditorHistoryList.Entry> entries = EDITOR_HISTORY_LIST.entries;

            for (var i = 0; i < entries.Count; i++)
            {
                entries[i].Refresh();
            }
        }
    }
}