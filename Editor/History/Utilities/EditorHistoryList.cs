﻿using System;
using System.Collections.Generic;
using System.IO;
using Module.NavigationTool.Editor.Utilities;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Module.NavigationTool.Editor.History
{
    [Serializable]
    internal sealed class EditorHistoryList
    {
        public static readonly string PREF_ID = "PREF_HISTORY_LIST";
        public const int MAX_LENGTH = 64;

        public List<Entry> pinnedEntries;
        public List<Entry> entries;
        
        public EditorHistoryList()
        {
            pinnedEntries = new List<Entry>();
            entries = new List<Entry>();
            string json = EditorProjectPrefs.GetString(PREF_ID);

            if (!string.IsNullOrEmpty(json))
                EditorJsonUtility.FromJsonOverwrite(json, this);
        }
        
        public void Add(string guid)
        {
            if (InternalAddEntry(new Entry(guid)))
                Save();
        }
        
        private bool InternalAddEntry(Entry e)
        {
            e.Refresh();
            
            if (!e.valid)
                return false;
            
            entries.Add(e);
            
            if (entries.Count > MAX_LENGTH)
                entries.RemoveRange(0, entries.Count - MAX_LENGTH);
            
            return true;
        }

        public void AddRange(string[] guids)
        {
            var isDirty = false;
            
            for (var i = 0; i < guids.Length; i++)
            {
                if (InternalAddEntry(new Entry(guids[i])))
                    isDirty = true;
            }

            if (isDirty)
                Save();
        }

        public void AddRangeByPath(string[] paths)
        {
            var isDirty = false;
            
            for (var i = 0; i < paths.Length; i++)
            {
                string guid = AssetDatabase.AssetPathToGUID(paths[i]);

                if (InternalAddEntry(new Entry(guid)))
                    isDirty = true;
            }

            if (isDirty)
                Save();
        }

        public void Remove(Entry e)
        {
            if (entries.Remove(e))
                Save();
        }
        
        public void Clear()
        {
            entries.Clear();
            Save();
        }
        
        public void Pin(Entry entry)
        {
            if (IsPinned(entry))
                return;

            pinnedEntries.Add(entry);
            Save();
        }

        public void Unpin(Entry entry)
        {
            int index = IndexOfPinned(entry);

            if (index == -1)
                return;

            pinnedEntries.RemoveAt(index);
            Save();
        }

        private bool IsPinned(Entry entry)
        {
            return IndexOfPinned(entry) != -1;
        }

        private int IndexOfPinned(Entry entry)
        {
            for (var i = 0; i < pinnedEntries.Count; i++)
            {
                if (pinnedEntries[i].guid.Equals(entry.guid))
                    return i;
            }

            return -1;
        }
        
        public Object GetObject(Entry entry)
        {
            string path = AssetDatabase.GUIDToAssetPath(entry.guid);
            return AssetDatabase.LoadMainAssetAtPath(path);
        }
        
        public void Save()
        {
            string json = EditorJsonUtility.ToJson(this, false);
            EditorProjectPrefs.SetString(PREF_ID, json);
        }

        /// <summary>
        /// Class: Entry
        /// </summary>
        [Serializable]
        public sealed class Entry : ISerializationCallbackReceiver
        {
            public string guid;
            
            [NonSerialized]
            public string name;
            [NonSerialized]
            public bool valid;
            [NonSerialized]
            public GUIContent content;
            [NonSerialized]
            public string lowerName;

            public Entry(string guid)
            {
                this.guid = guid;
            }
            
            void ISerializationCallbackReceiver.OnBeforeSerialize()
            {
            }

            void ISerializationCallbackReceiver.OnAfterDeserialize()
            {
                Refresh();
            }

            public void Refresh()
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                name = AssetDatabase.IsValidFolder(path) ? Path.GetFileName(path) : Path.GetFileNameWithoutExtension(path);
                lowerName = !string.IsNullOrEmpty(name) ? name.ToLower() : string.Empty;
                valid = AssetDatabase.LoadMainAssetAtPath(path) != null;
                content = new GUIContent(name, EditorHistoryGUIUtility.GetIcon(path), path);
            }
        }
    }
}