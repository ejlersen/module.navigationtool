﻿using UnityEditor;

namespace Module.NavigationTool.Editor.History
{
    [InitializeOnLoad]
    internal sealed class EditorHistoryListPostProcess : AssetPostprocessor
    {
        private static bool IGNORE_NEXT_SELECTION_CHANGE;

        static EditorHistoryListPostProcess()
        {
            Selection.selectionChanged -= OnSelectionChanged;
            Selection.selectionChanged += OnSelectionChanged;
        }

        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            bool isDirty = deletedAssets.Length != 0 ||
                           movedAssets.Length != 0 ||
                           movedFromAssetPaths.Length != 0;

            if (isDirty && EditorHistoryUtility.IsLoaded())
                EditorHistoryUtility.RefreshAll();
        }

        public static void IgnoreNextSelectionChange()
        {
            IGNORE_NEXT_SELECTION_CHANGE = true;
        }

        private static void OnSelectionChanged()
        {
            if (Selection.assetGUIDs == null)
                return;

            if (IGNORE_NEXT_SELECTION_CHANGE)
            {
                IGNORE_NEXT_SELECTION_CHANGE = false;
            }
            else
            {
                EditorHistoryList editorHistoryList = EditorHistoryUtility.GetHistoryList();
                editorHistoryList.AddRange(Selection.assetGUIDs);
                EditorHistoryGUIUtility.Repaint();
            }
        }
    }
}