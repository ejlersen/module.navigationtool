﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Module.NavigationTool.Editor.History
{
    internal sealed class EditorHistoryWindow : EditorWindow, IHasCustomMenu
    {
        [NonSerialized]
        private Styles styles;
        [SerializeField]
        public EditorHistoryViewTools viewTools;
        [SerializeField]
        private EditorHistoryViewSearchList viewSearchList;
        
        [MenuItem("Tools/Windows/History")]
        public static void Open()
        {
            var window = GetWindow<EditorHistoryWindow>();
            window.titleContent = new GUIContent("History");
            window.Show();
        }

        private void OnEnable()
        {
            if (styles == null)
                styles = new Styles();
            if (viewTools == null)
                viewTools = new EditorHistoryViewTools();
            if (viewSearchList == null)
                viewSearchList = new EditorHistoryViewSearchList();
        }

        private void OnGUI()
        {
            if (Event.current.type == EventType.Layout)
                return;
            
            styles.Initialize(GUI.skin);
            var rectTools = new Rect(0.0f, 4.0f, position.width, EditorGUIUtility.singleLineHeight);
            var rectList = new Rect(0.0f, rectTools.yMax + 4.0f, position.width, position.height - rectTools.height - 8.0f);
            
            viewTools.Initialize();
            viewTools.Draw(this, rectTools, styles);

            viewSearchList.Initialize();
            viewSearchList.Draw(this, rectList, styles);

            HandleClickEvent();
            
            if (Event.current.type == EventType.Repaint && position.Contains(Event.current.mousePosition))
                Repaint();
        }

        private void HandleClickEvent()
        {
            if (!EditorHistoryGUIUtility.isClicked)
                return;
            
            EditorHistoryList editorHistoryList = EditorHistoryUtility.GetHistoryList();
            
            if (EditorHistoryGUIUtility.isPinClicked)
            {
                if (EditorHistoryGUIUtility.isCurrentEntryPinned)
                    editorHistoryList.Unpin(EditorHistoryGUIUtility.currentEntry);
                else
                    editorHistoryList.Pin(EditorHistoryGUIUtility.currentEntry);
            }
            else
            {
                Object obj = editorHistoryList.GetObject(EditorHistoryGUIUtility.currentEntry);

                if (obj != null)
                {
                    if (EditorHistoryGUIUtility.isDoubleClick)
                    {
                        AssetDatabase.OpenAsset(obj);
                    }
                    else if (EditorHistoryGUIUtility.isRightClick)
                    {
                        EditorGUIUtility.PingObject(obj);
                    }
                    else
                    {
                        EditorHistoryListPostProcess.IgnoreNextSelectionChange();
                    
                        if (EditorHistoryPrefs.IsSetAsActiveObjectEnabled)
                            Selection.activeObject = obj;
                    
                        EditorGUIUtility.PingObject(obj);
                    }
                }
            }
            
            EditorHistoryGUIUtility.Used();
        }

        void IHasCustomMenu.AddItemsToMenu(GenericMenu menu)
        {
            menu.AddItem(new GUIContent("Clear"), 
                false, 
                () => EditorHistoryUtility.GetHistoryList()?.Clear());
            
            menu.AddSeparator("");

            menu.AddItem(new GUIContent("Active object in selection"), 
                EditorHistoryPrefs.IsSetAsActiveObjectEnabled, 
                () => EditorHistoryPrefs.IsSetAsActiveObjectEnabled = !EditorHistoryPrefs.IsSetAsActiveObjectEnabled);
        }
    }
}