﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.History
{
    [Serializable]
    internal sealed class EditorHistoryViewSearchList : AbstractEditorHistoryView
    {
        [SerializeField]
        private Vector2 scrollPosition;
        
        [NonSerialized]
        private EditorHistoryList editorHistoryList;

        public override void Initialize()
        {
            editorHistoryList = EditorHistoryUtility.GetHistoryList();
        }

        public override void Draw(EditorHistoryWindow window, Rect rect, Styles styles)
        {
            float entryHeight = EditorGUIUtility.singleLineHeight;
            float height = entryHeight * editorHistoryList.entries.Count;

            bool isSearching = !string.IsNullOrEmpty(window.viewTools.searchStr);
            string lowerSearchStr = window.viewTools.searchStr.ToLower();
            
            GUI.BeginGroup(rect);
            {
                var position = new Rect(0.0f, 0.0f, rect.width, rect.height);
                var viewRect = new Rect(0.0f, 0.0f, position.height > height ? position.width : position.width - 14.0f, height);
                var entryRect = new Rect(0.0f, 0.0f, viewRect.width, entryHeight);
            
                scrollPosition = GUI.BeginScrollView(position, scrollPosition, viewRect);
                {
                    for (int i = editorHistoryList.pinnedEntries.Count - 1; i >= 0; i--)
                    {
                        EditorHistoryList.Entry e = editorHistoryList.pinnedEntries[i];
                        
                        if (isSearching && !e.lowerName.Contains(lowerSearchStr))
                            continue;
                        
                        EditorHistoryGUIUtility.DrawEntry(entryRect, editorHistoryList.pinnedEntries[i], styles, true);
                        entryRect.y += entryHeight;
                    }
                    
                    for (int i = editorHistoryList.entries.Count - 1; i >= 0; i--)
                    {
                        EditorHistoryList.Entry e = editorHistoryList.entries[i];
                        
                        if (isSearching && !e.lowerName.Contains(lowerSearchStr))
                            continue;
                        
                        EditorHistoryGUIUtility.DrawEntry(entryRect, editorHistoryList.entries[i], styles, false);
                        entryRect.y += entryHeight;
                    }
                }
                GUI.EndScrollView();
            }
            GUI.EndGroup();
        }
    }
}