﻿using UnityEngine;

namespace Module.NavigationTool.Editor.History
{
    internal abstract class AbstractEditorHistoryView
    {
        public abstract void Initialize();
        public abstract void Draw(EditorHistoryWindow window, Rect rect, Styles styles);
    }
}