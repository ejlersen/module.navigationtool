﻿using System;
using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.History
{
    [Serializable]
    internal sealed class EditorHistoryViewTools : AbstractEditorHistoryView
    {
        public string searchStr = string.Empty;
        
        public override void Initialize()
        {
        }

        public override void Draw(EditorHistoryWindow window, Rect rect, Styles styles)
        {
            const float PADDING = 4.0f;
            const float ICON_SIZE = 20.0f;
            const float BUTTON_SIZE = 50.0f;
            
            GUI.BeginGroup(rect, styles.toolbox);
            {
                var r0 = new Rect(PADDING, 0.0f, ICON_SIZE, rect.height);
                var r1 = new Rect(r0.xMax, 0.0f, rect.width - BUTTON_SIZE - ICON_SIZE - PADDING * 2.0f, rect.height);
                var r2 = new Rect(r1.xMax, 0.0f, BUTTON_SIZE, rect.height);
                
                GUI.Label(r0, styles.iconSearch);
                searchStr = EditorGUI.TextField(r1, searchStr);

                if (GUI.Button(r2, "Clear"))
                    searchStr = string.Empty;
            }
            GUI.EndGroup();
        }

        public bool IsSearching()
        {
            return !string.IsNullOrEmpty(searchStr);
        }
    }
}