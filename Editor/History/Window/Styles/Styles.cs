﻿using UnityEditor;
using UnityEngine;

namespace Module.NavigationTool.Editor.History
{
    internal sealed class Styles
    {
        public GUIStyle toolbox;

        public GUIStyle icon;
        public GUIStyle entry;
        public GUIStyle invalidEntry;

        public GUIContent iconSearch;
        public GUIContent iconPinned;
        public GUIContent iconUnpinned;

        public Color colorNone;
        public Color colorToggleOff;
        public Color colorToggleOn;
        
        private GUISkin skin;

        public void Initialize(GUISkin skin)
        {
            if (this.skin == skin)
                return;

            this.skin = skin;
            toolbox = new GUIStyle(skin.box);

            icon = new GUIStyle
            {
                imagePosition = ImagePosition.ImageOnly,
                padding = new RectOffset(4, 0, 1, 0)
            };

            entry = new GUIStyle(skin.label);
            entry.hover.textColor = entry.onHover.textColor = Color.white;
            entry.active.textColor = entry.onActive.textColor = Color.yellow;
            
            invalidEntry = new GUIStyle(entry);
            invalidEntry.normal.textColor = Color.red;
            invalidEntry.hover.textColor = invalidEntry.onHover.textColor = new Color(1.0f, 0.3f, 0.3f);
            invalidEntry.active.textColor = invalidEntry.onActive.textColor = new Color(1.0f, 0.0f, 0.5f);

            iconSearch = EditorGUIUtility.IconContent("d_Search Icon");
            iconPinned = EditorGUIUtility.IconContent("d_Favorite");
            iconUnpinned = EditorGUIUtility.IconContent("Favorite");

            colorNone = new Color(1.0f, 1.0f, 1.0f, 0.0f);
            colorToggleOff = new Color(1.0f, 1.0f, 1.0f, 0.5f);
            colorToggleOn = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }
}