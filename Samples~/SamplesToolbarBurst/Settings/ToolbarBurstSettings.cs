using Module.NavigationTool.Editor.Toolbar;
using UnityEditor;

namespace Module.NavigationTool.Editor.Burst.Toolbar
{
    internal sealed class ToolbarBurstSettings : IToolbarSettings
    {
        public string Title => "Burst";
        
        private const string PREF_PREFS_ENABLED = "ToolbarSettings.IsBurstsEnabled";
        
        public static bool IsEnabled
        {
            get => EditorPrefs.GetBool(PREF_PREFS_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_PREFS_ENABLED, value);
        }
        
        public void Initialize()
        {
        }

        public void Draw()
        {
            IsEnabled = EditorGUILayout.Toggle("Enable Burst", IsEnabled);
        }
    }
}