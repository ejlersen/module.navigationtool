using System;
using System.Reflection;
using JetBrains.Annotations;
using Module.NavigationTool.Editor.Toolbar;
using UnityEditor;
using UnityEditor.Profiling;
using UnityEngine;

namespace Module.NavigationTool.Editor.Burst.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolBurst : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarBurstSettings.IsEnabled;
        public override bool Enabled => true;
        public override EToolbarPlacement Placement => EToolbarPlacement.Left;
        public override int Priority => (int)EToolbarPriority.Low;
        
        protected override void Draw(Rect rect)
        {
            Styles.Initialize();
            
            if (GUI.Button(rect, IsBurstEnabled() ? Styles.ICON_ENABLED : Styles.ICON_DISABLED, styles.button))
                EditorApplication.ExecuteMenuItem("Jobs/Burst/Enable Compilation");

            GUI.Label(rect, Styles.LABEL, styles.labelCenter);
        }

        public override float CalculateWidth()
        {
            return 24.0f;
        }

        private bool IsBurstEnabled()
        {
            const string typeFullname = "Unity.Burst.Editor.BurstEditorOptions, Unity.Burst";
            const string propertyName = "EnableBurstCompilation";
            
            var type = Type.GetType(typeFullname, false, true);

            if (type == null)
            {
                Debug.LogWarningFormat("Failed to find type with name: {0}", typeFullname);
                return false;
            }

            PropertyInfo property = type.GetProperty(propertyName, BindingFlags.Public | BindingFlags.Static);

            if (property == null)
            {
                Debug.LogWarningFormat("Failed to find property with name: {0} on type: {1}", propertyName, typeFullname);
                return false;
            }

            return (bool)property.GetValue(null);
        }
        
        private static class Styles
        {
            public static readonly GUIContent LABEL = new GUIContent(string.Empty, "Enable/disable burst compiler");

            public static GUIContent ICON_ENABLED;
            public static GUIContent ICON_DISABLED;
            
            private static bool IS_INITIALIZED;
            private const string EditorIconFileNameLight = "tex_icon_burst_light";
            private const string EditorIconFileNameDark = "tex_icon_burst_dark";

            public static void Initialize()
            {
                if (IS_INITIALIZED)
                    return;
                
                ICON_ENABLED = new GUIContent(LoadIcon("_enabled"));
                ICON_DISABLED = new GUIContent(LoadIcon("_disabled"));
                IS_INITIALIZED = true;
            }
            
            private static Texture2D LoadIcon(string postfix)
            {
                string iconPrefix = EditorGUIUtility.isProSkin ? EditorIconFileNameDark : EditorIconFileNameLight;
                string filename = $"{iconPrefix}{postfix}";
                string[] guids = AssetDatabase.FindAssets("t:texture " + filename);

                for (int i = 0; i < guids.Length; i++)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                    Texture2D tex = AssetDatabase.LoadAssetAtPath<Texture2D>(path);

                    if (tex != null)
                        return tex;
                }
                
                return null;
            }
        }
    }
}