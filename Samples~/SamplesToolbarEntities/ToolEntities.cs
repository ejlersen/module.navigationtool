using System;
using JetBrains.Annotations;
using Module.NavigationTool.Editor.Toolbar;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Module.NavigationTool.Editor.Entities.Toolbar
{
    [UsedImplicitly]
    internal sealed class ToolEntities : AbstractToolbarDrawer
    {
        public override bool Visible => ToolbarEntitiesSettings.IsEnabled;
        public override bool Enabled => true;
        public override EToolbarPlacement Placement => EToolbarPlacement.Left;
        public override int Priority => (int)EToolbarPriority.Low;
        
        protected override void Draw(Rect rect)
        {
            Styles.Initialize();

            Rect rect0 = new Rect(rect.x, rect.y, 24f, rect.height);
            Rect rect1 = new Rect(rect0.xMax, rect.y, 24f, rect.height);
            Rect rect2 = new Rect(rect1.xMax, rect.y, 24f, rect.height);
            Rect rect3 = new Rect(rect2.xMax, rect.y, 24f, rect.height);
            Rect rect4 = new Rect(rect3.xMax, rect.y, 24f, rect.height);

            if (GUI.Button(rect0, Styles.ICON_HIERARCHY, styles.button))
                ToggleWindow("HierarchyWindow");
            if (GUI.Button(rect1, Styles.ICON_COMPONENTS, styles.button))
                ToggleWindow("ComponentsWindow");
            if (GUI.Button(rect2, Styles.ICON_SYSTEMS, styles.button))
                ToggleWindow("SystemScheduleWindow");
            if (GUI.Button(rect3, Styles.ICON_ARCHETYPES, styles.button))
                ToggleWindow("ArchetypesWindow");
            if (GUI.Button(rect4, Styles.ICON_JOURNALING, styles.button))
                ToggleWindow("EntitiesJournalingWindow");
            
            GUI.Label(rect0, Styles.LABEL_HIERARCHY, styles.labelCenter);
            GUI.Label(rect1, Styles.LABEL_COMPONENTS, styles.labelCenter);
            GUI.Label(rect2, Styles.LABEL_SYSTEMS, styles.labelCenter);
            GUI.Label(rect3, Styles.LABEL_ARCHETYPES, styles.labelCenter);
            GUI.Label(rect4, Styles.LABEL_JOURNALING, styles.labelCenter);
        }

        public override float CalculateWidth()
        {
            return 120.0f;
        }
        
        private void ToggleWindow(string typeName)
        {
            string fullname = "Unity.Entities.Editor." + typeName + ", Unity.Entities.Editor";
            Type type = Type.GetType(fullname, false, true);

            if (type != null)
            {
                Object[] all = Resources.FindObjectsOfTypeAll(type);
                
                if (all.Length != 0)
                {
                    for (int i = 0; i < all.Length; i++)
                    {
                        if (all[i] is EditorWindow window)
                            window.Close();
                    }
                }
                else
                {
                    EditorWindow.GetWindow(type);
                }
            }
            else
            {
                Debug.LogWarningFormat("Failed to find type with name: {0}", fullname);
            }
        }

        private static class Styles
        {
            public static readonly GUIContent LABEL_HIERARCHY = new GUIContent(string.Empty, "Show/Hide entities hierarchy window");
            public static readonly GUIContent LABEL_COMPONENTS = new GUIContent(string.Empty, "Show/Hide entities components window");
            public static readonly GUIContent LABEL_SYSTEMS = new GUIContent(string.Empty, "Show/Hide entities systems window");
            public static readonly GUIContent LABEL_ARCHETYPES = new GUIContent(string.Empty, "Show/Hide entities archetypes window");
            public static readonly GUIContent LABEL_JOURNALING = new GUIContent(string.Empty, "Show/Hide entities journaling window");

            public static GUIContent ICON_HIERARCHY;
            public static GUIContent ICON_COMPONENTS;
            public static GUIContent ICON_SYSTEMS;
            public static GUIContent ICON_ARCHETYPES;
            public static GUIContent ICON_JOURNALING;
            
            private static bool IS_INITIALIZED;
            
            private const string EditorDefaultResourcesPath = "Packages/com.unity.entities/Editor Default Resources/";
            private const string EditorIconsLightDirectory = EditorDefaultResourcesPath + "icons/light";
            private const string EditorIconsDarkDirectory = EditorDefaultResourcesPath + "icons/dark";

            public static void Initialize()
            {
                if (IS_INITIALIZED)
                    return;
                
                ICON_HIERARCHY = new GUIContent(LoadIcon("EntityGroup/EntityGroup"));
                ICON_COMPONENTS = new GUIContent(LoadIcon("Component/Component"));
                ICON_SYSTEMS = new GUIContent(LoadIcon("System/System"));
                ICON_ARCHETYPES = new GUIContent(LoadIcon("Archetype/Archetype"));
                ICON_JOURNALING = new GUIContent(LoadIcon("Journaling/Journaling"));
                
                IS_INITIALIZED = true;
            }
            
            private static Texture2D LoadIcon(string name)
            {
                string iconsDirectory = EditorIconsLightDirectory;
                
                if (EditorGUIUtility.isProSkin)
                    iconsDirectory = EditorIconsDarkDirectory;
                
                Object obj = AssetDatabase.LoadAssetAtPath(iconsDirectory + "/" + name + ".png", typeof(Texture2D));
                return obj != null ? (Texture2D)obj : null;
            }
        }
    }
}