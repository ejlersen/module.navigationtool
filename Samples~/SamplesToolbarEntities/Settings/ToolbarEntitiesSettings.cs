using Module.NavigationTool.Editor.Toolbar;
using UnityEditor;

namespace Module.NavigationTool.Editor.Entities.Toolbar
{
    internal sealed class ToolbarEntitiesSettings : IToolbarSettings
    {
        public string Title => "Entities";
        
        private const string PREF_PREFS_ENABLED = "ToolbarSettings.IsEntitiesEnabled";
        
        public static bool IsEnabled
        {
            get => EditorPrefs.GetBool(PREF_PREFS_ENABLED, false);
            set => EditorPrefs.SetBool(PREF_PREFS_ENABLED, value);
        }
        
        public void Initialize()
        {
        }

        public void Draw()
        {
            IsEnabled = EditorGUILayout.Toggle("Enable Entities", IsEnabled);
        }
    }
}